## Risikoanalyse 

**Fehlendes Wissen/fehlende Erfahrung im Bereich der benutzten Konzepte, Technologien, Sprachen, usw.**   
Bei dem Softwarepraktikum reicht das bisherige Wissen der Teammitglieder meist nicht aus, da oft neue Themen und Bereiche gefragt sind, um die Aufgabe erfolgreich zu lösen. Dies setzt voraus, dass sich jedes Teammitglied selbstständig einarbeitet. So kann es zu großen Schwierigkeiten kommen, indem man zum Beispiel die fürs Einarbeiten erforderliche Zeit unterschätzt und dadurch aufgrund von fehlendem Verständnis Probleme verschärft, oder neue schafft.  
 
Lösungsansätze: Jeder Einzelne sollte nicht leichtsinnig mit den neuen Themen und 
Bereichen umgehen und sich rechtzeitig einarbeiten. Zusätzlich bietet das Team auch 
Methoden zur Bewältigung des Problems. So können beispielsweise Probleme über eine WhatsApp-Gruppe oder über Slack direkt angesprochen werden. Auch wird durch eine sinnvolle Rollenverteilung ein Ansprechpartner festgelegt. Fragen sollten also ohne zu zögern ausgesprochen werden. Weiterhin sollten aber auch die angebotenen Tutorien im Rahmen des Moduls besucht werden, um zusätzliches Wissen zu erwerben.   

**Schlechtes/Falsches Zeitmanagement**   
Durch feste Termine hinsichtlich der jeweiligen Abgaben ist der Zeitdruck bei der 
Bearbeitung der Aufgaben keine Seltenheit. So gerät man durch Fehlschätzungen beim Aufwand oder durch schlechte Aufgabenverteilung oft in Stresssituationen, die häufig Einfluss auf die Qualität des Projektes haben.   

Lösungsansätze: Durch eine sinnvolle Zerlegung in Issues und gleichmäßige 
Aufgabenverteilungen kann Stress vor der Deadline einer Abgabe entschärft werden. Auch das Einhalten der wöchentlichen Meetings mit dem Betreuer gewährleistet einen besseren 
Überblick über die erforderlichen Aufgaben der nächsten Woche. Durch eine regelmäßige Auswertung der Aufwandsberichte können auch Anpassungen für eine gerechtere Zeiteinteilung vorgenommen werden. 
 
**Probleme mit Server & Infrastruktur**   
Durch zum Beispiel den Ausfall eines Servers kann der Zugriff auf Ressourcen des Teams beeinträchtigt werden (z.B. Git, Slack). Dies kann das Gelingen des Praktikums deutlich erschweren. Da wir an einer Webseite arbeiten, kann es bei der Verfügbarkeit außerhalb des lokalen Rechners auch zu Ausfällen und Störungen kommen.   

Lösungsansätze: Vor Ausfällen dieser Art kann man sich quasi nicht schützen, da sie nicht durch das Team hervorgerufen werden, sondern durch externe Probleme. Man kann jedoch damit verbundenen Schaden durch Back-Ups minimieren (Repositorykopien im GitLab lokal speichern). 
 
**Schlechte Kommunikation innerhalb des Teams**   
Unklare Absprachen untereinander können dazu führen, dass Aufgaben falsch interpretiert oder priorisiert werden, die weniger wichtig sind als andere. Dies kann zum Verlust von Zeit führen, die man in anderen Teilen des Projekts gut utilisieren könnte. Wird nicht ausreichend miteinander kommuniziert führt dies häufig zu Missverständnissen, die sich dann auch in Form von Unzufriedenheit auf die Teammitglieder auswirken.   

Lösungsansätze: Jeder sollte versuchen, sich so klar wie möglich auszudrücken. Generell sollten Probleme oder Fragen in der Gruppe angesprochen werden. Besonders die WhatsApp-Gruppe oder Slack bieten sich dafür an. Bei möglichen Missverständnissen sollte man sich nicht scheuen, erneut nachzuhaken, um im Klaren zu bleiben. Bei den wöchentlichen Meetings oder anderen unplanmäßigen Treffen kann man auch Probleme an- und besprechen.
 
**Ungleiche Arbeitsteilung**   
Bei der Einteilung der Teammitglieder in verschiedene Rollen wie z.B. Front- und BackEnd, sowie bei der Aufteilung der Aufgaben wie der Risikoanalyse, kommt es dazu, dass einige Teammitglieder deutlich mehr Arbeitsaufwand haben als die anderen. Dies ist nicht nur unfair, sondern entspricht zugleich auch nicht dem Sinn des Praktikums.   

Lösungsansätze: Man protokolliert von Anfang an die zeitlichen Aufwände jedes Mitgliedes inklusive Schwierigkeitsgrad. Dadurch kann eine solche ungleiche Arbeitsteilung rechtzeitig erkannt und gegebenenfalls Rollen teilweise oder falls nötig komplett um-/neuverteilt werden. 
 
**Falsche Aufgabenschwerpunkte**    
Der Fokus wird möglicherweise auf die Umsetzung unwichtiger oder zu aufwändiger Features gelegt. Dadurch entsteht Zeitdruck und man vernachlässigt oft die wirklich wichtigen Kernpunkte. Dies geschieht beispielsweise durch mangelndes Wissen um die Entwicklung der Software, oder um der Anforderung einer einzelnen Aufgabe.   

Lösungsansätze: Man sollte sich umfangreich mit den zu bearbeitenden Aufgaben und den dazugehörigen Thematiken beschäftigen und sich besonders auch innerhalb der Gruppe sorgfältig dazu besprechen. So kann man genauer einschätzen, wie viel Zeit man mit einer Aufgabe verbringen muss und wie schwer die Aufgabe zu lösen ist. Auch sollte man die Hinweise des Betreuers und des Tutors zu bestehenden Problemen/Fragen berücksichtigen. 
Falls sich die bisherige Planung als sehr ungünstig aufweist, können oder sollten fehlerhafte 
Konzepte verworfen werden um Platz für neue Denkansätze zu schaffen, um aus einer 
Sackgasse zu entkommen. 
 
**Probleme im Teamzusammenhalt**   
Andere Punkte wie z.B. Probleme im Zeitmanagement oder Uneinigkeit über die Umsetzung führen zu einer Streitsituation in der Gruppe, die zum einen die Kommunikation und zum anderen die weitere Entwicklung erheblich behindern können, womit das ganze Projekt gefährdet ist.   

Lösungsansätze: Von Anfang an bei der Planung als gesamte Gruppe kommunizieren und falls notwendig mehrheitlich über einen uneinigen Punkt abstimmen, sodass das Risiko dieser Situation von Anfang an minimiert wird und es im Nachhinein nicht zu Problemen kommt, die bei vernünftiger Planung nicht entstanden wären. Der Ausschluss einer Person aus der Gruppe sollte nur in Extremfällen erwogen werden, da dies zu anderen Problemen führen wird. (siehe ‚Ausfall beteiligter Personen‘) 
 
**Ausfall beteiligter Personen**   
Eine an dem Praktikum beteiligte Person fällt aus verschiedenen Gründen (z.B. aus gesundheitlichen Gründen oder durch Ausschluss) komplett oder auch nur vorübergehend aus. Somit muss die gesamte Arbeit dieser Person auf die restlichen aufgeteilt werden.   

Lösungsansätze: Rechtzeitige Rücksprache mit dem Gruppenleiter, um sich möglichst früh auf dieses Problem vorzubereiten. Bei Bedarf noch eine zusätzliche Rücksprache mit dem Tutor. 
 
**Funktionsuntüchtiger Code**   
Ein Teil des Programms wurde bereits als fertig deklariert, jedoch stellt sich im weiteren Verlauf des Praktikums raus, dass dieser gar nicht funktionstüchtig war, was das Team nun vor große Hürden stellt: Die Fehlersuche gestaltet sich schwierig, da dieser unter Umständen schon einige Zeit zurückliegt oder bei Änderung ein neues Problem hervorruft.   

Lösungsansätze: Bevor etwas in das Git geladen wird, sollte es gründlich auf Fehler geprüft und zugleich ausreichend kommentiert werden. So wird zum einen das Risiko für Fehler reduziert und zum anderen die Fehlersuche- und Behebung deutlich erleichtert. 
 
**Uneinigkeit über die Umsetzung des Programms**   
Das Team wird sich über gewisse Punkte der Programmplanung und -entwicklung nicht einig und kann somit nicht an der Projektumsetzung weiterarbeiten, wodurch es beispielsweise zeitliche und gruppendynamische Probleme gibt.   

Lösungsansätze: Es kann ein erneuter Abspracheversuch des Teams unternommen werden. Sollte dieser scheitern, besteht die Möglichkeit nach sinnvoller Abwägung der unstimmigen Planungspunkte innerhalb der Gruppe abzustimmen. Scheitert auch dies, sollte der Gruppenleiter eine Entscheidung fällen. Dies sollte hinsichtlich der Fristen zeitlich schnell passieren.

[//]: * (Bezüglich Punkt 1: Was ist genau gemeint mit "Ansprechpartner fest"? Könnte m.M nach etwas genauer  geschrieben werden )
[//]: * (Bezüglich Punkt "Funktionsuntücheriger Code": Sollte es nicht auch eine Möglichkeit geben mit Continuous Integration den Code zu testen? Eventuell kann man dies auch notieren, anstatt es nur lokal zu testen, falls wir das auch verwenden wollen)
[//]: * (Bezüglich Letzten Punkt: "Innerhalb der Gruppe abstimmen" würde ich präzisieren mit "abstimmen, welche Punkte für das Projekt wertvoll sind" oder ähnliches.)
