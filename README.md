This project is part of a laboratory course in the winter semester 19/20 at the University of Leipzig. This project is 
about implementing a messenger service and client based on the Mercure protocol and a REST API. In the back end, 
PHP with Symfony and API Platform is used, the front end is a web app based on React.js.  
In addition to messaging, the web app allows users to see their physical distance to other users.  
  
Prerequisites:  
You need to have Docker and Docker-Compose installed on your local machine.  
  
Quick Start:  
1. `cd $YOUR_PROJECT_DIR/api-platform-2.5.0`  
2. `docker-compose up -d` (starts docker container)  
3. `docker-compose exec php bin/console doctrine:fixtures:load` (loads first admin/ user into database)  
   1. admin: email=admin@dev.com, password=admin123
   2. user1: email=user1@dev.com, password=user1234
   3. user2: email=user2@dev.com, password=user2345  
4. Now you can open https://localhost:443/login to access the login page of the web app, or you can go to 
http://localhost:8080/ to access the backend by means of Swagger  
  
Important Notes:  
* Admin accounts are not allowed the use the chat feature (they have to create their own user account for that)
* After you generated your token in Swagger with the /authentication_token endpoint you need to use the format 
"Bearer $YOUR_TOKEN" in the form of the "Authorize" button to authenticate yourself 