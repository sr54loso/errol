<?php

namespace App\Tests\Unit\SecurityTest;

use App\Entity\AdminUser;
use App\Entity\Chat;
use App\Entity\Message;
use App\Entity\NormalUser;
use App\Security\ChatVoter;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Security;

/*
This class tests the ChatVoter
@author Stefan Rosenlund
*/
class ChatVoterTest extends TestCase
{
    protected $chatVoter;
    protected $supports;
    protected $voteOnAttribute;
    protected $securityStub;
    protected $tokenStub;

    // Called Once before every method
    protected function setUp(): void
    {
        $this->securityStub = $this->createMock(Security::class);
        $this->chatVoter = new ChatVoter($this->securityStub);
        $this->supports = new \ReflectionMethod('App\Security\ChatVoter', 'supports');
        $this->supports->setAccessible(true);

        $this->voteOnAttribute = new \ReflectionMethod('App\Security\ChatVoter', 'voteOnAttribute');
        $this->voteOnAttribute->setAccessible(true);
        $this->tokenStub = $this->createMock(TokenInterface::class);
    }

    /**
     * @doesNotPerformAssertions
     */
    public function viewDeleteEditProvider()
    {
        return [['view'], ['delete'], ['edit']];
    }

    /**
     * @doesNotPerformAssertions
     */
    public function postDeleteViewProvider()
    {
        return [['view'], ['delete'], ['post']];
    }

    public function testSupportsChat(): void
    {
        $this->assertTrue($this->supports->invoke($this->chatVoter, 'post', new Chat()));
    }

    public function testSupportsTraversable(): void
    {
        $traversableStub = $this->createMock(\Traversable::class);

        $this->assertTrue($this->supports->invoke($this->chatVoter, 'view', $traversableStub));
    }

    public function testSupportsNotPatch(): void
    {
        $this->assertFalse($this->supports->invoke($this->chatVoter, 'patch', new Chat()));
    }

    public function testSupportsNotMessage(): void
    {
        $this->assertFalse($this->supports->invoke($this->chatVoter, 'post', new Message()));
    }

    public function testVoteOnAttributePostGranted(): void
    {
        $this->tokenStub->method('getUser')->willReturn(new NormalUser());

        $this->assertTrue($this->voteOnAttribute->invoke($this->chatVoter, 'post', new Chat(), $this->tokenStub));
    }

    public function testVoteOnAttributePostDenied(): void
    {
        $this->tokenStub->method('getUser')->willReturn(null);

        $this->assertFalse($this->voteOnAttribute->invoke($this->chatVoter, 'post', new Chat(), $this->tokenStub));
    }

    /**
     * @dataProvider viewDeleteEditProvider
     */
    public function testVoteOnAttributeViewGranted($method): void
    {
        $subscriber = new NormalUser();
        $this->tokenStub->method('getUser')->willReturn($subscriber);

        $this->assertTrue($this->voteOnAttribute->invoke($this->chatVoter, $method, (new Chat())->addUser($subscriber), $this->tokenStub));
    }

    /**
     * @dataProvider viewDeleteEditProvider
     */
    public function testVoteOnAttributeDeleteDenied($method): void
    {
        $this->tokenStub->method('getUser')->willReturn(new NormalUser());

        $this->assertFalse($this->voteOnAttribute->invoke($this->chatVoter, $method, new Chat(), $this->tokenStub));
    }

    /**
     * @dataProvider postDeleteViewProvider
     */
    public function testVoteOnAttributeAdminGranted(): void
    {
        $this->securityStub->method('isGranted')->will($this->returnValueMap([['ROLE_ADMIN', null, true]]));
        $this->tokenStub->method('getUser')->willReturn(new AdminUser());

        $this->assertTrue($this->voteOnAttribute->invoke($this->chatVoter, 'view', new Chat(), $this->tokenStub));
    }

    public function testVoteOnAttributeEditAdminDenied(): void
    {
        $this->securityStub->method('isGranted')->will($this->returnValueMap([['ROLE_ADMIN', null, true]]));
        $this->tokenStub->method('getUser')->willReturn(new AdminUser());

        $this->assertFalse($this->voteOnAttribute->invoke($this->chatVoter, 'edit', new Chat(), $this->tokenStub));
    }
}
