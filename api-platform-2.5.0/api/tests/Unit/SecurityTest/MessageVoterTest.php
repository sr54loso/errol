<?php

namespace App\Tests\Unit\SecurityTest;

use App\Entity\AdminUser;
use App\Entity\Chat;
use App\Entity\Message;
use App\Entity\NormalUser;
use App\Security\MessageVoter;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Security;

/*
This class tests the MessageVoter
@author Stefan Rosenlund
*/
class MessageVoterTest extends TestCase
{
    protected $messageVoter;
    protected $supports;
    protected $voteOnAttribute;
    protected $securityStub;
    protected $tokenStub;
    protected $message;

    // Called Once before every method
    protected function setUp(): void
    {
        $this->securityStub = $this->createMock(Security::class);
        $this->messageVoter = new MessageVoter($this->securityStub);
        $this->supports = new \ReflectionMethod('App\Security\MessageVoter', 'supports');
        $this->supports->setAccessible(true);

        $this->voteOnAttribute = new \ReflectionMethod('App\Security\MessageVoter', 'voteOnAttribute');
        $this->voteOnAttribute->setAccessible(true);
        $this->tokenStub = $this->createMock(TokenInterface::class);

        $this->message = (new Message())->setChat(new Chat());
    }

    /**
     * @doesNotPerformAssertions
     */
    public function postViewProvider()
    {
        return [['view'], ['post']];
    }

    /**
     * @doesNotPerformAssertions
     */
    public function editDeleteProvider()
    {
        return [['edit'], ['delete']];
    }

    /**
     * @doesNotPerformAssertions
     */
    public function deleteViewProvider()
    {
        return [['view'], ['delete']];
    }

    /**
     * @doesNotPerformAssertions
     */
    public function postEditProvider()
    {
        return [['post'], ['edit']];
    }

    public function testSupportsMessage(): void
    {
        $this->assertTrue($this->supports->invoke($this->messageVoter, 'post', new Message()));
    }

    public function testSupportsTraversable(): void
    {
        $traversableStub = $this->createMock(\Traversable::class);

        $this->assertTrue($this->supports->invoke($this->messageVoter, 'view', $traversableStub));
    }

    public function testSupportsNotPatch(): void
    {
        $this->assertFalse($this->supports->invoke($this->messageVoter, 'patch', new Message()));
    }

    public function testSupportsNotChat(): void
    {
        $this->assertFalse($this->supports->invoke($this->messageVoter, 'post', new Chat()));
    }

    /**
     * @dataProvider postViewProvider
     */
    public function testVoteOnAttributePostGranted($method): void
    {
        $subscriber = new NormalUser();
        $this->message->getChat()->addUser($subscriber);
        $this->tokenStub->method('getUser')->willReturn($subscriber);

        $this->assertTrue($this->voteOnAttribute->invoke($this->messageVoter, $method, $this->message, $this->tokenStub));
    }

    /**
     * @dataProvider postViewProvider
     */
    public function testVoteOnAttributeViewDenied($method): void
    {
        $this->tokenStub->method('getUser')->willReturn(new NormalUser());

        $this->assertFalse($this->voteOnAttribute->invoke($this->messageVoter, $method, $this->message, $this->tokenStub));
    }

    /**
     * @dataProvider editDeleteProvider
     */
    public function testVoteOnAttributeEditGranted($method): void
    {
        $subscriber = new NormalUser();
        $this->message->getChat()->addUser($subscriber);
        $this->tokenStub->method('getUser')->willReturn($subscriber);

        $this->assertTrue($this->voteOnAttribute->invoke($this->messageVoter, $method, $this->message->setSender($subscriber), $this->tokenStub));
    }

    /**
     * @dataProvider editDeleteProvider
     */
    public function testVoteOnAttributeDeleteDenied($method): void
    {
        $this->tokenStub->method('getUser')->willReturn(new NormalUser());

        $this->assertFalse($this->voteOnAttribute->invoke($this->messageVoter, $method, $this->message, $this->tokenStub));
    }

    /**
     * @dataProvider deleteViewProvider
     */
    public function testVoteOnAttributeAdminGranted($method): void
    {
        $this->securityStub->method('isGranted')->will($this->returnValueMap([['ROLE_ADMIN', null, true]]));
        $this->tokenStub->method('getUser')->willReturn(new AdminUser());

        $this->assertTrue($this->voteOnAttribute->invoke($this->messageVoter, $method, $this->message, $this->tokenStub));
    }

    /**
     * @dataProvider postEditProvider
     */
    public function testVoteOnAttributeEditAdminDenied($method): void
    {
        $this->securityStub->method('isGranted')->will($this->returnValueMap([['ROLE_ADMIN', null, true]]));
        $this->tokenStub->method('getUser')->willReturn(new AdminUser());

        $this->assertFalse($this->voteOnAttribute->invoke($this->messageVoter, $method, $this->message, $this->tokenStub));
    }
}
