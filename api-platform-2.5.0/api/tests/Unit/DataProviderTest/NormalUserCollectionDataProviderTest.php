<?php

namespace App\Tests\Unit\DataProviderTest;

use App\DataProvider\NormalUserCollectionDataProvider;
use App\Entity\AdminUser;
use App\Entity\NormalUser;
use App\Test\CustomKernelTestCase;
use Doctrine\ORM\EntityManagerInterface;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;
use Symfony\Bridge\Doctrine\ManagerRegistry;
use Symfony\Component\Security\Core\Security;

/*
This class tests the UserDataProvider
@author Stefan Rosenlund
*/
class NormalUserCollectionDataProviderTest extends CustomKernelTestCase
{
    // Clears database between every test
    use ReloadDatabaseTrait;

    protected $dataProvider;
    protected $entityManager;
    protected $securityStub;
    protected $managerRegistry;
    protected $collectionExtensions;

    // Called Once before every method
    protected function setUp(): void
    {
        self::bootKernel();
        $this->entityManager = self::$container->get(EntityManagerInterface::class);
        $this->managerRegistry = $this->createMock(ManagerRegistry::class);
        $this->managerRegistry->method(('getManagerForClass'))->willReturn($this->entityManager);
        $this->collectionExtensions = [];
        $this->securityStub = $this->createMock(Security::class);
    }

    /**
     * @return array
     * @doesNotPerformAssertions
     */
    public function supportsFalseProvider()
    {
        $adminStub = $this->createMock(AdminUser::class);
        $normalUserStub = $this->createMock(NormalUser::class);

        return [[$adminStub, 'App\Entity\NormalUser'], [$normalUserStub, 'App\Entity\AdminUser'], [$adminStub, 'App\Entity\AdminUser']];
    }

    public function testSupportsTrue(): void
    {
        $requestUserStub = $this->createMock(NormalUser::class);
        $this->securityStub->method('getUser')->willReturn($requestUserStub);
        $this->dataProvider = new NormalUserCollectionDataProvider($this->entityManager, $this->securityStub,
            $this->managerRegistry, $this->collectionExtensions);

        $this->assertTrue($this->dataProvider->supports('App\Entity\NormalUser'));
    }

    /**
     * @dataProvider supportsFalseProvider
     */
    public function testSupportsFalse($requestUser, $databaseClass): void
    {
        $this->securityStub->method('getUser')->willReturn($requestUser);
        $this->dataProvider = new NormalUserCollectionDataProvider($this->entityManager, $this->securityStub,
            $this->managerRegistry, $this->collectionExtensions);

        $this->assertFalse($this->dataProvider->supports($databaseClass));
    }

    public function testGetCollection()
    {
        $requestUser = $this->createUser(false, 'normal1@gmx.de', 'normal1@gmx.de', 0, 0);
        $this->createUser(false, 'normal2@gmx.de', 'normal2@gmx.de', 10, 10);
        $this->createUser(false, 'normal3@gmx.de', 'normal3@gmx.de', 20, 20);

        $this->securityStub->method('getUser')->willReturn($requestUser);
        $this->dataProvider = new NormalUserCollectionDataProvider($this->entityManager, $this->securityStub,
            $this->managerRegistry, $this->collectionExtensions);

        $generator = $this->dataProvider->getCollection('App\Entity\NormalUser');

        foreach ($generator as $fetchedUser) {
            $this->assertInstanceOf(NormalUser::class, $fetchedUser);
            if ($fetchedUser === $requestUser) {
                $this->assertEquals(0, $fetchedUser->getDistance());
            } else {
                $this->assertGreaterThan(0, $fetchedUser->getDistance());
                $this->assertLessThan(3500000, $fetchedUser->getDistance());
            }
        }
    }
}
