<?php

namespace App\Tests\Unit\SerializerTest;

use App\Serializer\UserNameConverter;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Unit test for App\Serializer\UserNameConverter.
 *
 * @author Louis Fahrenholz
 */
class UserNameConverterTest extends KernelTestCase
{
    protected $userNameConverter;

    /**
     * Executed once before every testing function in this class.
     * Creates new UserNameConverter.
     */
    protected function setUp(): void
    {
        $this->UserNameConverter = new UserNameConverter();
    }

    /**
     * Tests normalize().
     * Function call should return password.
     */
    public function testNormalizePlainPassword()
    {
        $result = $this->UserNameConverter->normalize('plainPassword');
        $this->assertEquals('password', $result);
    }

    /**
     * Tests normalize().
     * Function call should return Zaphod.
     */
    public function testNormalizeNotPlainPassword()
    {
        $result = $this->UserNameConverter->normalize('Zaphod');
        $this->assertNotEquals('password', $result);
    }

    /**
     * Tests denormalize().
     * Function call should return plainPassword.
     */
    public function testDenormalizePassword()
    {
        $result = $this->UserNameConverter->denormalize('password');
        $this->assertEquals('plainPassword', $result);
    }

    /**
     * Tests denormalize().
     * Function call should return Beeblebrox.
     */
    public function testDenormalizeNotPassword()
    {
        $result = $this->UserNameConverter->denormalize('Beeblebrox');
        $this->assertNotEquals('plainPassword', $result);
    }
}
