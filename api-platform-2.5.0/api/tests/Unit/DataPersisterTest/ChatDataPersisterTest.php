<?php

namespace App\Tests\Unit\DataPersisterTest;

use App\DataPersister\ChatDataPersister;
use App\Entity\Chat;
use App\Entity\Message;
use App\Test\CustomKernelTestCase;
use Doctrine\ORM\EntityManagerInterface;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;
use Symfony\Component\Security\Core\Security;

/*
This class tests the ChatDataPersister
@author Stefan Rosenlund
*/
class ChatDataPersisterTest extends CustomKernelTestCase
{
    // Clears database between every test
    use ReloadDatabaseTrait;

    protected $dataPersister;
    protected $entityManager;
    protected $securityStub;

    // Called Once before every method
    protected function setUp(): void
    {
        $this->securityStub = $this->createMock(Security::class);
        self::bootKernel();
        $this->entityManager = self::$container->get(EntityManagerInterface::class);
        $this->dataPersister = new ChatDataPersister($this->entityManager, $this->securityStub);
    }

    public function testSupportsChat(): void
    {
        $this->assertTrue($this->dataPersister->supports(new Chat()));
    }

    public function testSupportsNotMessage(): void
    {
        $this->assertFalse($this->dataPersister->supports(new Message()));
    }

    public function testPersistChatSubscriber(): void
    {
        $subscriber = $this->createUser(false, 'user@gmx.de', 'password1');

        $chat = new Chat();
        $chat->setName('test');
        $this->securityStub->method('getUser')->willReturn($subscriber);
        $this->dataPersister->persist($chat, ['collection_operation_name' => 'post']);

        $fetchedChat = $this->entityManager->getRepository(Chat::class)->find($chat->getId());

        $this->assertInstanceOf(Chat::class, $fetchedChat);
        $this->assertContains($subscriber, $fetchedChat->getUsers());
    }
}
