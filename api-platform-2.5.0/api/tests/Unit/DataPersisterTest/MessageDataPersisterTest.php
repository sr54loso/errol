<?php

namespace App\Tests\Unit\DataPersisterTest;

use App\DataPersister\MessageDataPersister;
use App\Entity\Chat;
use App\Entity\Message;
use App\Test\CustomKernelTestCase;
use Doctrine\ORM\EntityManagerInterface;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;
use Symfony\Component\Security\Core\Security;

/*
This class tests the MessageDataPersister
@author Stefan Rosenlund
*/
class MessageDataPersisterTest extends CustomKernelTestCase
{
    // Clears database between every test
    use ReloadDatabaseTrait;

    protected $dataPersister;
    protected $entityManager;
    protected $securityStub;

    // Called Once before every method
    protected function setUp(): void
    {
        $this->securityStub = $this->createMock(Security::class);
        self::bootKernel();
        $this->entityManager = self::$container->get(EntityManagerInterface::class);
        $this->dataPersister = new MessageDataPersister($this->entityManager, $this->securityStub);
    }

    public function testSupportsMessage(): void
    {
        $this->assertTrue($this->dataPersister->supports(new Message()));
    }

    public function testSupportsNotChat(): void
    {
        $this->assertFalse($this->dataPersister->supports(new Chat()));
    }

    public function testPersistChatSubscriber(): void
    {
        $sender = $this->createUser(false, 'user@gmx.de', 'password1');
        $chat = $this->createChat('TestChat', [$sender]);

        $message = new Message();
        $message->setContent('test');
        $message->setChat($chat);
        $this->securityStub->method('getUser')->willReturn($sender);
        $this->dataPersister->persist($message, ['collection_operation_name' => 'post']);

        $fetchedMessage = $this->entityManager->getRepository(Message::class)->find($message->getId());

        $this->assertInstanceOf(Message::class, $fetchedMessage);
        $this->assertEquals($sender, $fetchedMessage->getSender());
    }
}
