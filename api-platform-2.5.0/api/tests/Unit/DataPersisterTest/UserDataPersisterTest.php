<?php

namespace App\Tests\Unit\DataPersisterTest;

use App\DataPersister\NormalUserDataPersister;
use App\Entity\Message;
use App\Entity\NormalUser;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/*
This class tests the UserDataPersister
@author Stefan Rosenlund
*/
class UserDataPersisterTest extends KernelTestCase
{
    // Clears database between every test
    use ReloadDatabaseTrait;

    protected $dataPersister;
    protected $entityManager;

    // Called Once before every method
    protected function setUp(): void
    {
        self::bootKernel();
        $this->entityManager = self::$container->get(EntityManagerInterface::class);
        $pe = self::$container->get(UserPasswordEncoderInterface::class);
        $this->dataPersister = new NormalUserDataPersister($this->entityManager, $pe);
    }

    public function testSupportsUser(): void
    {
        $userStub = $this->createMock(NormalUser::class);

        $this->assertTrue($this->dataPersister->supports($userStub));
    }

    public function testSupportsNotMessage(): void
    {
        $this->assertFalse($this->dataPersister->supports(new Message()));
    }

    // check exception and $data
    public function testPersistEncodedPasswort(): void
    {
        $user = new NormalUser();
        $user->setEmail('user@gmx.de');
        $user->setUsername('user');
        $user->setPlainPassword('password1');

        $this->dataPersister->persist($user);

        $fetchedUser = $this->entityManager->getRepository(User::class)->find($user->getId());

        $this->assertInstanceOf(NormalUser::class, $fetchedUser);
        $this->assertNotNull($fetchedUser);
        $this->assertNotEquals('password1', $fetchedUser->getPassword());
    }
}
