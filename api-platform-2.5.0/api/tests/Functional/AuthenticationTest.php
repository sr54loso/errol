<?php

namespace App\Tests\Functional;

use App\Test\CustomApiTestCase;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;

class AuthenticationTest extends CustomApiTestCase
{
    // Cleares database
    use ReloadDatabaseTrait;

    public function testAuthentication()
    {
        // Creates HTTP client
        $client = self::createClient();

        $user = $this->createUser(true, 'test1@gmx.de', 'password1');

        // first: successful authentication, second: failed authentication

        $response = $client->request('POST', 'authentication_token', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'email' => $user->getEmail(),
                'password' => $user->getPlainPassword(),
            ],
        ]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertArrayHasKey('token', $response->toArray());

        $response2 = $client->request('POST', 'authentication_token', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'email' => $user->getEmail(),
                'password' => 'false',
            ],
        ]);

        $this->assertResponseStatusCodeSame(401);
    }
}
