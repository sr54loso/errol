<?php

namespace App\Tests\Functional;

use App\Entity\AdminUser;
use App\Entity\NormalUser;
use App\Test\CustomApiTestCase;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;

class UserTest extends CustomApiTestCase
{
    // Clears database between every test
    use ReloadDatabaseTrait;

    private $adminUser;
    private $normalUser;
    private $client;
    private $adminToken;

    public function __construct()
    {
        parent::__construct();

        // this adminUser is used througout the whole test
        $this->adminUser = new AdminUser();
        $this->adminUser->setEmail('admintest@gmx.de');
        $this->adminUser->setPlainPassword('adminPassword1');
        $this->adminUser->setUsername('admintest');

        // this normalUser is used througout the whole test
        $this->normalUser = new NormalUser();
        $this->normalUser->setEmail('normaltest@gmx.de');
        $this->normalUser->setPlainPassword('normalpassword1');
        $this->normalUser->setUsername('normaltest');
    }

    public function testPostUser(): void
    {
        $client = self::createClient();
        $adminToken = $this->createUserandAuthenticate($client, true, $this->adminUser->getEmail(), $this->adminUser->getPlainPassword());
        $response = $client->request('POST', '/users', [
            'headers' => [
                'Authorization' => 'Bearer '.$adminToken,
            ],
            'json' => [
                'email' => $this->normalUser->getEmail(),
                'username' => $this->normalUser->getUsername(),
                'password' => $this->normalUser->getPlainPassword(),
            ],
        ]);

        $this->assertResponseStatusCodeSame(201);
        // Asserts that the returned JSON is validated by the JSON Schema generated for this resource by API Platform
        $this->assertMatchesResourceItemJsonSchema(NormalUser::class);
    }

    public function testGetUserCollection(): void
    {
        $client = self::createClient();
        $adminToken = $this->createUserandAuthenticate($client, true, $this->adminUser->getEmail(), $this->adminUser->getPlainPassword());
        $response = $client->request('GET', '/admins', [
            'headers' => [
                'Authorization' => 'Bearer '.$adminToken,
            ],
        ]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertMatchesResourceCollectionJsonSchema(AdminUser::class);
    }

    public function testPutUser(): void
    {
        $client = self::createClient();

        $user = $this->createUser(true, $this->adminUser->getEmail(), $this->adminUser->getPlainPassword());
        $adminToken = $this->authenticate($client, $user->getEmail(), $user->getPlainPassword());

        $response = $client->request('PUT', '/admins/'.$user->getId(), [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer '.$adminToken,
            ],
            'json' => [
                'username' => $user->getUsername().'new',
            ],
        ]);

        $this->assertResponseStatusCodeSame(200);
        // Asserts that the returned JSON is a superset of this one
        /*$this->assertJsonContains([
            'username' => $user->getUsername()."new",
        ]);*/
    }

    /*
    public function testDeleteUser(): void
    {
        $client = self::createClient();
        $adminToken = $this->createUserandAuthenticate($client, true, $this->adminUser->getEmail(), $this->adminUser->getPlainPassword());

        $normalUser = $this->createUser(false, $this->normalUser->getEmail(), $this->normalUser->getPlainPassword());

        $response = $client->request('DELETE', '/users/'.$normalUser->getId(), [
            'headers' => [
                'Authorization' => 'Bearer '.$adminToken,
            ],
        ]);

        $this->assertResponseStatusCodeSame(204);
    }

    public function testGetUser(): void
    {
        $client = self::createClient();
        $adminToken = $this->createUserandAuthenticate($client, true, $this->adminUser->getEmail(), $this->adminUser->getPlainPassword());

        $normalUser = $this->createUser(false, $this->normalUser->getEmail(), $this->normalUser->getPlainPassword());

        $response = $client->request('GET', '/users/'.$normalUser->getId(), [
            'headers' => [
                'Authorization' => 'Bearer '.$adminToken,
            ],
        ]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertMatchesResourceItemJsonSchema(NormalUser::class);
    }
     **/
}
