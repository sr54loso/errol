<?php

namespace App\Serializer;

use Symfony\Component\Serializer\NameConverter\NameConverterInterface;

/**
 * Converts property names of User objects.
 *
 * @author Louis Fahrenholz
 */
class UserNameConverter implements NameConverterInterface
{
    /**
     * {@inheritdoc}
     *
     * @return string 'plainPassword' will be converted to 'password'
     */
    public function normalize($propertyName): string
    {
        return ('plainPassword' !== $propertyName) ? $propertyName : 'password';
    }

    /**
     * {@inheritdoc}
     *
     * @return string 'password' will be converted to 'plainPassword'
     */
    public function denormalize($propertyName): string
    {
        return ('password' !== $propertyName) ? $propertyName : 'plainPassword';
    }
}
