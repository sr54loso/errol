<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200301052716 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE "user_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE message_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chat_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE normalUser (id INT NOT NULL, location geography(GEOMETRY, 4326) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE adminUser (id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, email VARCHAR(254) NOT NULL, username VARCHAR(64) NOT NULL, password VARCHAR(128) NOT NULL, discr VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649F85E0677 ON "user" (username)');
        $this->addSql('CREATE INDEX username_idx ON "user" (username)');
        $this->addSql('CREATE TABLE message (id INT NOT NULL, chat_id INT DEFAULT NULL, sender_id INT DEFAULT NULL, type VARCHAR(32) DEFAULT \'text\' NOT NULL, content VARCHAR(140) NOT NULL, timestamp TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B6BD307F1A9A7125 ON message (chat_id)');
        $this->addSql('CREATE INDEX IDX_B6BD307FF624B39D ON message (sender_id)');
        $this->addSql('COMMENT ON COLUMN message.timestamp IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE chat (id INT NOT NULL, name VARCHAR(64) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE chat_normal_user (chat_id INT NOT NULL, normal_user_id INT NOT NULL, PRIMARY KEY(chat_id, normal_user_id))');
        $this->addSql('CREATE INDEX IDX_A76323B11A9A7125 ON chat_normal_user (chat_id)');
        $this->addSql('CREATE INDEX IDX_A76323B1DE39982E ON chat_normal_user (normal_user_id)');
        $this->addSql('ALTER TABLE normalUser ADD CONSTRAINT FK_38E549D0BF396750 FOREIGN KEY (id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE adminUser ADD CONSTRAINT FK_16CD33F8BF396750 FOREIGN KEY (id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F1A9A7125 FOREIGN KEY (chat_id) REFERENCES chat (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307FF624B39D FOREIGN KEY (sender_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chat_normal_user ADD CONSTRAINT FK_A76323B11A9A7125 FOREIGN KEY (chat_id) REFERENCES chat (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chat_normal_user ADD CONSTRAINT FK_A76323B1DE39982E FOREIGN KEY (normal_user_id) REFERENCES normalUser (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE chat_normal_user DROP CONSTRAINT FK_A76323B1DE39982E');
        $this->addSql('ALTER TABLE normalUser DROP CONSTRAINT FK_38E549D0BF396750');
        $this->addSql('ALTER TABLE adminUser DROP CONSTRAINT FK_16CD33F8BF396750');
        $this->addSql('ALTER TABLE message DROP CONSTRAINT FK_B6BD307FF624B39D');
        $this->addSql('ALTER TABLE message DROP CONSTRAINT FK_B6BD307F1A9A7125');
        $this->addSql('ALTER TABLE chat_normal_user DROP CONSTRAINT FK_A76323B11A9A7125');
        $this->addSql('DROP SEQUENCE "user_id_seq" CASCADE');
        $this->addSql('DROP SEQUENCE message_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chat_id_seq CASCADE');
        $this->addSql('DROP TABLE normalUser');
        $this->addSql('DROP TABLE adminUser');
        $this->addSql('DROP TABLE "user"');
        $this->addSql('DROP TABLE message');
        $this->addSql('DROP TABLE chat');
        $this->addSql('DROP TABLE chat_normal_user');
    }
}
