<?php

namespace App\EventListener;

use App\Entity\AdminUser;
use App\Entity\NormalUser;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;

/**
 * Event listener and handler for AuthenticationSuccessEvents.
 *
 * @author Louis Fahrenholz
 */
class AuthenticationSuccessListener
{
    private $domain;

    /**
     * Mercure secret key.
     * Used for signing the JWTs.
     *
     * @var string
     */
    private $mercureSecretKey;

    public function __construct($mercureSecretKey = '', $domain = 'http://localhost:8080')
    {
        $this->domain = $domain;
        $this->mercureSecretKey = $mercureSecretKey;
    }

    /**
     * Generates the JWT needed for subscriptions to the mercure hub and adds it to the response payload of the /authentication_token endpoint.
     * The JWT can be found under the key 'mercure' in the payload. Also adds this users IRI to the response.
     *
     * @return void
     */
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $user = $event->getUser();
        $subscribe = [];
        if ($user instanceof AdminUser) {
            $subscribe = ['*'];
        } elseif ($user instanceof NormalUser) {
            $subscribe = ["{$this->domain}/users/{$user->getId()}"];
        } else {
            return;
        }

        $token = (new Builder())
            ->set('mercure', ['subscribe' => $subscribe])
            ->sign(new Sha256(), $this->mercureSecretKey)
            ->getToken();

        $response = $event->getResponse();
        $response->headers->set(
            'set-cookie',
            sprintf('mercureAuthorization=%s; Path=/.well-known/mercure; httponly; secure; SameSite=strict', $token)
        );

        $data = $event->getData();
        $data['mercure'] = strval($token);
        $data['user_iri'] = ($user instanceof AdminUser) ? '/admins/'.$user->getId() : '/users/'.$user->getId();
        $data['username'] = $user->getUsername();
        $event->setData($data);
    }
}
