<?php

namespace App\Test;

use App\Entity\AdminUser;
use App\Entity\Chat;
use App\Entity\NormalUser;
use App\Entity\User;
use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * provides standard methods for unit tests.
 *
 * @author Stefan Rosenlund
 */
class CustomKernelTestCase extends KernelTestCase
{
    /**
     * creates a user and persists the user in the database.
     *
     * @param $admin true for creating admin and false for creating normalUser
     * @param $email email of new user, word before @ is username of new  user
     * @param $plainPassword password of new user
     *
     * @return persisted user
     */
    protected function createUser(bool $admin, string $email, string $plainPassword, float $latitude = 0, float $longitude = 0): User
    {
        if ($admin) {
            $user = new AdminUser();
        } else {
            $user = new NormalUser();
            $user->setLatitude($latitude);
            $user->setLongitude($longitude);
            $user->setLocation();
        }
        $user->setEmail($email);
        $user->setUsername(substr($email, 0, strpos($email, '@')));
        $user->setPlainPassword($plainPassword);
        $encoded = self::$container->get('security.password_encoder')
            ->encodePassword($user, $plainPassword);
        $user->setPassword($encoded);

        $em = self::$container->get('doctrine')->getManager();
        $em->persist($user);
        $em->flush();

        return $user;
    }

    protected function createChat(string $name, array $users = [])
    {
        $chat = new Chat();
        $chat->setName($name);
        $chat->setUsers($users);
        $chat->setLastModified(new DateTimeImmutable());

        $em = self::$container->get('doctrine')->getManager();
        $em->persist($chat);
        $em->flush();

        return $chat;
    }
}
