<?php

namespace App\Test;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;
use App\Entity\AdminUser;
use App\Entity\NormalUser;
use App\Entity\User;

/**
 * provides standard methods for functional tests.
 *
 * @author Stefan Rosenlund
 */
class CustomApiTestCase extends ApiTestCase
{
    /**
     * creates a user and persists the user in the database.
     *
     * @param $admin true for creating admin and false for creating normalUser
     * @param $email email of new user, word before @ is username of new  user
     * @param $plainPassword password of new user
     *
     * @return persisted user
     */
    protected function createUser(bool $admin, string $email, string $plainPassword): User
    {
        if ($admin) {
            $user = new AdminUser();
        } else {
            $user = new NormalUser();
        }
        $user->setEmail($email);
        $user->setUsername(substr($email, 0, strpos($email, '@')));
        $user->setPlainPassword($plainPassword);
        $encoded = self::$container->get('security.password_encoder')
            ->encodePassword($user, $plainPassword);
        $user->setPassword($encoded);

        $em = self::$container->get('doctrine')->getManager();
        $em->persist($user);
        $em->flush();

        return $user;
    }

    /**
     * requests JWT for specific user.
     *
     * @param $client HTTP client for requests
     * @param $email of user
     * @param $password of user
     *
     * @return JWT
     */
    protected function authenticate(Client $client, string $email, string $password): string
    {
        $response = $client->request('POST', 'authentication_token', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'email' => $email,
                'password' => $password,
            ],
        ]);

        return $response->toArray()['token'];
    }

    /**
     * creates user, persists that user and requests JWT.
     *
     * @param $client HTTP client for requests
     * @param $admin true for creating admin and false for creating normalUser
     * @param $email of user
     * @param $password of user
     *
     * @return JWT
     */
    protected function createUserandAuthenticate(Client $client, bool $admin, string $email, string $password): string
    {
        $this->createUser($admin, $email, $password);
        $token = $this->authenticate($client, $email, $password);

        return $token;
    }
}
