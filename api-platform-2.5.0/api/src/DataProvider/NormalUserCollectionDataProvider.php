<?php

namespace App\DataProvider;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryResultCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGenerator;
use ApiPlatform\Core\DataProvider\CollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\NormalUser;
use App\Entity\User;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;

final class NormalUserCollectionDataProvider implements CollectionDataProviderInterface, RestrictedDataProviderInterface
{
    private $em;
    private $requestUser;
    private $collectionExtensions;
    private $managerRegistry;

    public function __construct(EntityManagerInterface $em, Security $security, ManagerRegistry $managerRegistry, iterable $collectionExtensions)
    {
        $this->em = $em;

        $this->requestUser = $security->getUser();
        if (!$this->requestUser instanceof User) {
            throw new AuthenticationException();
        }

        $this->managerRegistry = $managerRegistry;
        $this->collectionExtensions = $collectionExtensions;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        if (!($this->requestUser instanceof NormalUser)) {
            return false;
        }

        return NormalUser::class == $resourceClass;
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = []): \Generator
    {
        $preparationQuery = $this->em->createQuery('SELECT u.location FROM App\Entity\NormalUser u WHERE u.id = :requestUserId');
        $preparationQuery->setParameter('requestUserId', $this->requestUser->getId());
        $requestUserLocation = $preparationQuery->getSingleScalarResult();

        $manager = $this->managerRegistry->getManagerForClass($resourceClass);
        $repository = $manager->getRepository($resourceClass);
        $queryBuilder = $repository->createQueryBuilder('user');

        if ($requestUserLocation) {
            // compute distance
            $queryBuilder->addSelect('ST_Distance(user.location, :requestUserLocation)');
            $queryBuilder->orderBy('ST_Distance(user.location, :requestUserLocation)', 'ASC');
            $queryBuilder->setParameter('requestUserLocation', $requestUserLocation);
        }

        $result = [];

        // Inject extensions (pagination, filter, etc)
        $queryNameGenerator = new QueryNameGenerator();
        foreach ($this->collectionExtensions as $extension) {
            $extension->applyToCollection($queryBuilder, $queryNameGenerator, $resourceClass, $operationName, $context);
            if ($extension instanceof QueryResultCollectionExtensionInterface && $extension->supportsResult($resourceClass, $operationName, $context)) {
                $result = $extension->getResult($queryBuilder, $resourceClass, $operationName, $context);
            }
        }

        foreach ($result as $databaseUser) {
            if (is_array($databaseUser)) {
                $distance = $databaseUser[1];
                $databaseUser = $databaseUser[0];
                $databaseUser->setDistance($distance);
            }
            foreach ($databaseUser->getChats() as $chat) {
                if (!$chat->inUsers($this->requestUser)) {
                    $databaseUser->removeChat($chat);
                }
            }
            yield $databaseUser;
        }
    }
}
