<?php

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\Chat;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Component\Security\Core\Security;

class ChatItemDataProvider implements ItemDataProviderInterface, RestrictedDataProviderInterface
{
    private $em;
    private $security;

    public function __construct(EntityManagerInterface $em, Security $security)
    {
        $this->em = $em;
        $this->security = $security;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Chat::class === $resourceClass;
    }

    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): ?Chat
    {
        $dql = 'SELECT c FROM App\Entity\Chat c WHERE c.id = ?1';
        $query = $this->em->createQuery($dql)->setParameter(1, $id);
        $chat = null;
        try {
            $chat = $query->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        } catch (NonUniqueResultException $e) {
            // if this happens, it means that there are multiple chat entities
            // in the database that have the same id.
            // This would be a serious database issue which should be logged
            // as a critical issue in the future.
            return null;
        }

        $chat->matchMessages(Criteria::create()->setFirstResult(0)->setMaxResults(30));

        return $chat;
    }
}
