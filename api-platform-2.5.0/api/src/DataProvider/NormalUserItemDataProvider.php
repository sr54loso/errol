<?php

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\NormalUser;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;

class NormalUserItemDataProvider implements ItemDataProviderInterface, RestrictedDataProviderInterface
{
    private $em;
    private $requestUser;

    public function __construct(EntityManagerInterface $em, Security $security)
    {
        $this->em = $em;

        $this->requestUser = $security->getUser();
        if (!$this->requestUser instanceof User) {
            throw new AuthenticationException();
        }
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        if (!($this->requestUser instanceof NormalUser)) {
            return false;
        }

        return NormalUser::class == $resourceClass;
    }

    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): ?NormalUser
    {
        if ($this->requestUser->getId() !== $id) {
            return $this->getOtherUser($id, $this->requestUser);
        } else {
            return $this->getOwnUser($id);
        }
    }

    private function getOtherUser($id, $requestUser): ?NormalUser
    {
        $dql = 'SELECT u1 AS user, ST_Distance(u1.location, u2.location) AS distance
                FROM App\Entity\NormalUser u1, App\Entity\NormalUser u2
                WHERE u1.id = ?1 AND u2.id = ?2';
        $query = $this->em->createQuery($dql)
            ->setParameter(1, $id)
            ->setParameter(2, $requestUser->getId());
        $result = null;
        try {
            $result = $query->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        } catch (NonUniqueResultException $e) {
            // if this happens, it means that there are multiple NormalUser entities
            // in the database that have the same id.
            // This would be a serious database issue which should be logged
            // as a critical issue in the future.
            return null;
        }
        $user = $result['user'];
        if (null !== $result['distance']) {
            $user->setDistance($result['distance']);
        }
        foreach ($user->getChats() as $chat) {
            if (!$chat->inUsers($requestUser)) {
                $user->removeChat($chat);
            }
        }

        return $user;
    }

    private function getOwnUser($id): ?NormalUser
    {
        $dql = 'SELECT u FROM App\Entity\NormalUser u WHERE u.id = ?1';
        $query = $this->em->createQuery($dql)->setParameter(1, $id);
        $user = null;
        try {
            $user = $query->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        } catch (NonUniqueResultException $e) {
            // if this happens, it means that there are multiple NormalUser entities
            // in the database that have the same id.
            // This would be a serious database issue which should be logged
            // as a critical issue in the future.
            return null;
        }

        return $user;
    }
}
