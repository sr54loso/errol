<?php

namespace App\DataProvider;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryResultCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGenerator;
use ApiPlatform\Core\DataProvider\CollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\Chat;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

class ChatCollectionDataProvider implements CollectionDataProviderInterface, RestrictedDataProviderInterface
{
    private $em;
    private $security;
    private $collectionExtensions;

    public function __construct(EntityManagerInterface $em, Security $security, iterable $collectionExtensions)
    {
        $this->em = $em;
        $this->security = $security;
        $this->collectionExtensions = $collectionExtensions;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Chat::class === $resourceClass;
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = []): iterable
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('c')
           ->from('App\Entity\Chat', 'c')
           ->orderBy('c.lastModified', 'DESC');

        $result = [];

        // Inject extensions (pagination, filter, etc)
        $queryNameGenerator = new QueryNameGenerator();
        foreach ($this->collectionExtensions as $extension) {
            $extension->applyToCollection($qb, $queryNameGenerator, $resourceClass, $operationName, $context);
            if ($extension instanceof QueryResultCollectionExtensionInterface && $extension->supportsResult($resourceClass, $operationName, $context)) {
                $result = $extension->getResult($qb, $resourceClass, $operationName, $context);
            }
        }

        foreach ($result as $chat) {
            $chat->matchMessages(Criteria::create()->setFirstResult(0)->setMaxResults(1));
        }

        return (count($result) > 0) ? $result : [];
    }
}
