<?php

namespace App\DataProvider;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryResultCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGenerator;
use ApiPlatform\Core\DataProvider\CollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\Message;
use Doctrine\ORM\EntityManagerInterface;

class MessageCollectionDataProvider implements CollectionDataProviderInterface, RestrictedDataProviderInterface
{
    private $em;
    private $collectionExtensions;

    public function __construct(EntityManagerInterface $em, iterable $collectionExtensions)
    {
        $this->em = $em;
        $this->collectionExtensions = $collectionExtensions;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Message::class === $resourceClass;
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = []): iterable
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('m')
           ->from($resourceClass, 'm')
           ->orderby('m.timestamp', 'DESC');

        $result = [];

        // Inject extensions (pagination, filter, etc)
        $queryNameGenerator = new QueryNameGenerator();
        foreach ($this->collectionExtensions as $extension) {
            $extension->applyToCollection($qb, $queryNameGenerator, $resourceClass, $operationName, $context);
            if ($extension instanceof QueryResultCollectionExtensionInterface && $extension->supportsResult($resourceClass, $operationName, $context)) {
                $result = $extension->getResult($qb, $resourceClass, $operationName, $context);
            }
        }

        return (count($result) > 0) ? $result : [];
    }
}
