<?php

namespace App\DataFixtures;

use App\Entity\AdminUser;
use App\Entity\NormalUser;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class UserFixtures extends Fixture
{
    private $encoderFactory;

    public function __construct(EncoderFactoryInterface $encoderFactory)
    {
        $this->encoderFactory = $encoderFactory;
    }

    public function load(ObjectManager $manager)
    {
        // create first admin
        $admin = new AdminUser();
        $admin->setEmail('admin@dev.com');
        $admin->setUsername('admin');

        $encoder = $this->encoderFactory->getEncoder(AdminUser::class);
        $admin->setPassword($encoder->encodePassword('admin123', null));

        $manager->persist($admin);

        // create first normal user
        $user1 = new NormalUser();
        $user1->setEmail('user1@dev.com');
        $user1->setUsername('user1');

        $encoder = $this->encoderFactory->getEncoder(NormalUser::class);
        $user1->setPassword($encoder->encodePassword('user1234', null));

        $manager->persist($user1);

        // create second normal user
        $user2 = new NormalUser();
        $user2->setEmail('user2@dev.com');
        $user2->setUsername('user2');

        $encoder = $this->encoderFactory->getEncoder(NormalUser::class);
        $user2->setPassword($encoder->encodePassword('user2345', null));

        $manager->persist($user2);

        $manager->flush();
    }
}
