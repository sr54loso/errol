<?php

namespace App\Entity;

/**
 * Admin user.
 *
 * @author Louis Fahrenholz
 */
class AdminUser extends User
{
    /**
     * {@inheritdoc}
     *
     * @return array always ['ROLE_USER', 'ROLE_ADMIN']
     */
    public function getRoles(): array
    {
        return ['ROLE_USER', 'ROLE_ADMIN'];
    }
}
