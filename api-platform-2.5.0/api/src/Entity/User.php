<?php

namespace App\Entity;

use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Basic user class for all roles.
 *
 * @author Louis Fahrenholz
 */
abstract class User implements UserInterface
{
    /**
     * Unique identifier.
     * Will be changed to UUID.
     *
     * @var int
     */
    private $id;

    /**
     * User email.
     * Should be unique.
     *
     * @var string
     */
    private $email;

    /**
     * Display name of user.
     *
     * @var string
     */
    private $username;

    /**
     * Hashed Password of user.
     *
     * @var string
     */
    private $password;

    /**
     * Plain text password of user.
     * Only set if the user is newly created or if the user changed the password.
     *
     * @var string|null
     */
    private $plainPassword = null;

    /**
     * Get unique user id.
     *
     * @return int user id
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Get user email adress.
     * Unique.
     *
     * @return string user email
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * Get username.
     * Not unique.
     *
     * @return string username
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * Get plain text password of user.
     *
     * @return string null if the user isn't newly created or the password changed
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /**
     * Get hashed password of user.
     *
     * @return string hashed password
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * Get user roles.
     *
     * @return array array of user roles
     */
    abstract public function getRoles(): array;

    /**
     * Set user Email.
     * Unique.
     *
     * @param string $email user email
     *
     * @return self this user
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Set username.
     * Not unique.
     *
     * @param string $username userame
     *
     * @return self this user
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Set user password.
     * TODO: validation against second password and add constraints.
     *
     * @param string $password user password
     *
     * @return self this user
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set plain text user password.
     * This should be used when creating an new user.
     *
     * @param string $plainPassword plain text user password
     *
     * @return self this user
     */
    public function setPlainPassword(string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * Not implemented.
     * Only defined because UserInterface requires it.
     */
    public function getSalt()
    {
    }

    /**
     * Set plainPassword to null.
     * Should be called after the plain password is not needed any more.
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    /**
     * Unique string representation of user object.
     * Just the user id casted to string.
     *
     * @return string unique string representing this user
     */
    public function __toString(): string
    {
        return (string) $this->id;
    }
}
