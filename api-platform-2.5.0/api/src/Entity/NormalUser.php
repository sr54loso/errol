<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Normal user.
 *
 * @author Louis Fahrenholz
 */
class NormalUser extends User
{
    /**
     * List of chats the user participates in.
     *
     * @var Collection
     */
    private $chats;

    /**
     * Latitude of user location, y-value.
     *
     * @var float|null
     *
     * @author Stefan Rosenlund
     */
    private $latitude;

    /**
     * Longitude of user location, x-value.
     *
     * @var float|null
     *
     * @author Stefan Rosenlund
     */
    private $longitude;

    /**
     * Last known location of user. Format: "POINT(x y)".
     *
     * @var string|null
     *
     * @author Stefan Rosenlund
     */
    private $location = null;

    /**
     * Distance to requesting user. Will be set by dataProvider.
     *
     * @var float
     *
     * @author Stefan Rosenlund
     */
    private $distance = null;

    public function __construct()
    {
        $this->chats = new ArrayCollection();
    }

    /**
     * {@inheritdoc}
     *
     * @return array always ['ROLE_USER']
     */
    public function getRoles(): array
    {
        return ['ROLE_USER'];
    }

    /**
     * Get longitude of user location.
     *
     * @return float longitude
     *
     * @author Stefan Rosenlund
     */
    public function getLongitude(): ?float
    {
        return (float) $this->longitude;
    }

    /**
     * Get latitude of user location.
     *
     * @return float latitude
     *
     * @author Stefan Rosenlund
     */
    public function getLatitude(): ?float
    {
        return (float) $this->latitude;
    }

    /**
     * Get last known location.
     *
     * @return string format: "POINT(x y)"
     *
     * @author Stefan Rosenlund
     */
    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function getChats(): array
    {
        return $this->chats->getValues();
    }

    public function getDistance(): ?float
    {
        return $this->distance;
    }

    public function setDistance(float $distance = null): self
    {
        $this->distance = $distance;

        return $this;
    }

    public function setChats(array $chats): self
    {
        $this->chats = new ArrayCollection($chats);

        return $this;
    }

    public function addChat(Chat $chat): self
    {
        $this->chats->add($chat);

        return this;
    }

    public function removeChat(Chat $chat): self
    {
        $this->chats->removeElement($chat);

        return $this;
    }

    /**
     * Set longitude of user location.
     *
     * @param float
     *
     * @return self this user
     *
     * @author Stefan Rosenlund
     */
    public function setLongitude(float $longitude = null): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Set latitude of user location.
     *
     * @param float
     *
     * @return self this user
     *
     * @author Stefan Rosenlund
     */
    public function setLatitude(float $latitude = null): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Set location of user.
     *
     * @return self this user
     *
     * @author Stefan Rosenlund
     */
    public function setLocation(): self
    {
        if (null === $this->latitude || null === $this->longitude) {
            $this->location = null;

            return $this;
        }
        $this->location = sprintf(
          'SRID=4326;POINT(%f %f)',
          $this->longitude,
          $this->latitude
        );

        return $this;
    }
}
