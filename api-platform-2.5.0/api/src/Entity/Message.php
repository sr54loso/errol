<?php

namespace App\Entity;

use DateTimeImmutable;

/**
 * This class implements Messages that users send in a specific chat.
 *
 * @author Louis Fahrenholz
 */
class Message
{
    /**
     * Unique message id.
     *
     * @var int
     */
    private $id;

    /**
     * Sender of this Message.
     *
     * @var User
     */
    private $sender;

    /**
     * Chat this message is in.
     *
     * @var Chat
     */
    private $chat;

    /**
     * Content of this Message.
     * For now a String, may change to binary.
     *
     * @var string
     */
    private $content;

    /**
     * Content type.
     * For now only 'text' is allowd, which is also the default.
     * Maybe implement subclasses for different message types.
     *
     * @var string
     */
    private $type = 'text';

    /**
     * Timestamp.
     * DateTimeImmutable object specifying the time that this message was created at.
     *
     * @var DateTimeImmutable
     */
    private $timestamp;

    /**
     * Get unique message id.
     *
     * @return int message id
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Get the sender/author of this message.
     *
     * @return User User object of the sender
     */
    public function getSender(): User
    {
        return $this->sender;
    }

    /**
     * Get the Chat object this message was sent to.
     *
     * @return Chat Chat object
     */
    public function getChat(): Chat
    {
        return $this->chat;
    }

    /**
     * Get the content of this message.
     *
     * @return string content string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * Get the content type of this message.
     *
     * @return string message type
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Set the content of this message.
     *
     * @param string $content content of this message
     *
     * @return self this message
     */
    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Set the chat this message is in.
     *
     * @param string $content chat this message is in
     *
     * @return self this message
     */
    public function setChat(Chat $chat): self
    {
        $this->chat = $chat;

        return $this;
    }

    /**
     * Set the the sender of this message.
     *
     * @param string $content sender of this message
     *
     * @return self this message
     */
    public function setSender(User $sender): self
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * Set the content type of this message.
     *
     * @param string $type content type
     *
     * @return self this message
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Set timestamp.
     *
     * @param DateTimeImmutable $timestamp timestamp
     *
     * @return self this message
     */
    public function setTimestamp(DateTimeImmutable $timestamp): self
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get timestamp.
     *
     * @return DateTimeImmutable timestamp
     */
    public function getTimestamp(): DateTimeImmutable
    {
        return $this->timestamp;
    }
}
