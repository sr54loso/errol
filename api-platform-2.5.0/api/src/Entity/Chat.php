<?php

namespace App\Entity;

use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;

/**
 * This class implements chats that users send messages to.
 *
 * @author Louis Fahrenholz
 */
class Chat
{
    /**
     * Unique chat id.
     *
     * @var int
     */
    private $id;

    /**
     * Chat name.
     *
     * @var string|null
     */
    private $name;

    /**
     * List of messages in this chat.
     *
     * @var Collection
     */
    private $messages;

    /**
     * List of users participating in this Chat.
     *
     * @var Collection
     */
    private $users;

    /**
     * Timestamp of the chat creation or of the most recent message depending on what is later.
     *
     * @var DateTimeImmutable
     */
    private $lastModified;

    /**
     * Constructor for this chat object.
     */
    public function __construct()
    {
        $this->name = null;
        $this->messages = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    /**
     * Get unique chat id.
     *
     * @return int unique chat id
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Get chat name.
     * The chat name is not unique and only a help for the user to identify chats. The default chat name is 'chat_<chat_id>' for now.
     *
     * @return string chat name
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Get collection of all messages in this chat.
     *
     * @return array message collection
     */
    public function getMessages(): array
    {
        return $this->messages->getValues();
    }

    /**
     * Get collection of all users participating in this chat.
     *
     * @return array user collection
     */
    public function getUsers(): array
    {
        return $this->users->getValues();
    }

    public function inUsers(NormalUser $user): bool
    {
        return \in_array($user, $this->getUsers(), true);
    }

    /**
     * Get collection of IRIs of all users participating in this chat.
     *
     * @param web domain of server
     *
     * @return array IRI collection
     *
     * @author Stefan Rosenlund
     */
    public function getUsersIri(string $serverDomain): array
    {
        return array_map(function ($element) use ($serverDomain) {
            return $serverDomain.'/users/'.$element->getId();
        }, $this->getUsers());
    }

    /**
     * Set the name of this chat.
     *
     * @param string $name new chat name
     *
     * @return self this chat object
     */
    public function setName(string $name = null): self
    {
        $this->name = $name;

        return $this;
    }

    public function matchMessages(Criteria $criteria): self
    {
        $this->messages = $this->messages->matching($criteria);

        return $this;
    }

    /**
     * Set the users in this chat.
     *
     * @param Collection $messages messages in this chat
     *
     * @return self this chat object
     */
    public function setMessages(array $messages): self
    {
        $this->messages = new ArrayCollection($messages);

        return $this;
    }

    /**
     * Set the users in this chat.
     *
     * @param Collection $users users in this chat
     *
     * @return self this chat object
     */
    public function setUsers(array $users): self
    {
        $this->users = new ArrayCollection($users);

        return $this;
    }

    public function setLastModified(DateTimeImmutable $timestamp): self
    {
        $this->lastModified = $timestamp;

        return $this;
    }

    public function addUser(NormalUser $user): self
    {
        $this->users->add($user);

        return $this;
    }

    public function removeUser(NormalUser $user): self
    {
        $this->users->removeElement($user);

        return $this;
    }

    public function convertName($username): self
    {
        $usernames = [];
        foreach ($this->getUsers() as $user) {
            \array_push($usernames, $user->getUsername());
        }

        $idx = \array_search($username, $usernames);
        if (false !== $idx) {
            \array_splice($usernames, $idx, 1);
        }

        $this->setName(\implode(', ', $usernames));

        return $this;
    }
}
