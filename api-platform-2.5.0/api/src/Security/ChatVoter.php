<?php

namespace App\Security;

use App\Entity\Chat;
use App\Entity\NormalUser;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

/**
 * This voter votes on operations regarding chats.
 *
 * @author Louis Fahrenholz
 */
class ChatVoter extends Voter
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * This voter supports chat objects or traversables containing chat objects and attributes post, view, edit and delete.
     * {@inheritdoc}
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, ['post', 'view', 'edit', 'delete'])) {
            return false;
        }

        if (!$subject instanceof Chat && !(is_iterable($subject) && 'view' == $attribute)) {
            return false;
        }

        if (is_iterable($subject)) {
            foreach ($subject as $item) {
                if (!$item instanceof Chat) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Decides if the user may perform an operation on the chat entity.
     * Every authorized (normal) user can create new chats. Admins and chat subscribers can delete the chat. Only chat subscribers can view and edit the chat.
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case 'post':
                return $this->canPost($user);
            case 'view':
                if (!is_iterable($subject)) {
                    return $this->canView($subject, $user);
                } else {
                    return $this->canViewList($subject, $user);
                }
            case 'edit':
                return $this->canEdit($subject, $user);
            case 'delete':
                return $this->canDelete($subject, $user);
            default:
                return false;
    }
    }

    private function canPost(User $user): bool
    {
        return $user instanceof NormalUser;
    }

    private function canView(Chat $chat, User $user): bool
    {
        if ($this->canEdit($chat, $user)) {
            return true;
        }

        return false;
    }

    private function canViewList(iterable $chats, User $user): bool
    {
        foreach ($chats as $chat) {
            if (!$this->canView($chat, $user)) {
                return false;
            }
        }

        return true;
    }

    private function canEdit(Chat $chat, User $user): bool
    {
        if (in_array($user, $chat->getUsers(), true)) {
            return true;
        }

        return false;
    }

    private function canDelete(Chat $chat, User $user): bool
    {
        if ($this->canView($chat, $user)) {
            return true;
        }

        if ($this->security->isGranted('ROLE_ADMIN')) {
            return true;
        }

        return false;
    }
}
