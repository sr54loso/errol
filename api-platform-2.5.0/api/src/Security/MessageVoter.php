<?php

namespace App\Security;

use App\Entity\Message;
use App\Entity\NormalUser;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

/**
 * This voter votes on operations regarding messages.
 *
 * @author Louis Fahrenholz
 */
class MessageVoter extends Voter
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * This voter supports message objects or traversables containing message objects and attributes post, view, edit and delete.
     * {@inheritdoc}
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, ['post', 'view', 'edit', 'delete'])) {
            return false;
        }

        if (!$subject instanceof Message && !(is_iterable($subject) && 'view' == $attribute)) {
            return false;
        }

        if (is_iterable($subject)) {
            foreach ($subject as $item) {
                if (!$item instanceof Message) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Decides if the user may perform an operation on the message entity.
     * (Normal) users in a chat can post messages to that chat. All users in a chat can view messages in a chat. Message senders that are in the chat can edit that message. Message senders that are in the chat can delete that message.
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof NormalUser) {
            return false;
        }

        switch ($attribute) {
      case 'post':
        return $this->canPost($subject, $user);
        break;
      case 'view':
        if (!is_iterable($subject)) {
            return $this->canView($subject, $user);
        } else {
            return $this->canViewList($subject, $user);
        }
        break;
      case 'edit':
        return $this->canEdit($subject, $user);
        break;
      case 'delete':
        return $this->canDelete($subject, $user);
        break;
      default:
        return false;
        break;
    }
    }

    private function canPost(Message $message, User $user): bool
    {
        if (in_array($user, $message->getChat()->getUsers(), true)) {
            return true;
        }

        return false;
    }

    private function canView(Message $message, User $user): bool
    {
        if ($this->canPost($message, $user)) {
            return true;
        }

        return false;
    }

    private function canViewList(iterable $messages, User $user): bool
    {
        foreach ($messages as $message) {
            if (!$this->canView($message, $user)) {
                return false;
            }
        }

        return true;
    }

    private function canEdit(Message $message, User $user): bool
    {
        if ($this->canPost($message, $user) && $user === $message->getSender()) {
            return true;
        }

        return false;
    }

    private function canDelete(Message $message, User $user): bool
    {
        if ($this->canEdit($message, $user)) {
            return true;
        }

        return false;
    }
}
