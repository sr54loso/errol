<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Message;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Data persister for Message objects.
 *
 * @author Louis Fahrenholz
 */
class MessageDataPersister implements ContextAwareDataPersisterInterface
{
    private $entityManager;
    private $security;

    public function __construct(EntityManagerInterface $entityManager, Security $security)
    {
        $this->entityManager = $entityManager;
        $this->security = $security;
    }

    /**
     * {@inheritdoc}
     */
    public function supports($data, array $context = []): bool
    {
        return $data instanceof Message;
    }

    /**
     * {@inheritdoc}
     */
    public function persist($data, array $context = [])
    {
        $data->setSender($this->security->getUser());

        if (isset($context['collection_operation_name']) && 'post' == $context['collection_operation_name']) {
            $data->setTimestamp(new DateTimeImmutable());
            $data->getChat()->setLastModified($data->getTimestamp());
        }

        $this->entityManager->persist($data);
        $this->entityManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function remove($data, array $context = [])
    {
        $this->entityManager->remove($data);
        $this->entityManager->flush();
    }
}
