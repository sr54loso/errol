<?php

namespace App\DataPersister;

use App\Entity\NormalUser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * NormalUser DataPersister.
 *
 * @author Louis Fahrenholz
 */
class NormalUserDataPersister extends AbstractUserDataPersister
{
    public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $userPasswordEncoder)
    {
        parent::__construct($entityManager, $userPasswordEncoder);
    }

    /**
     * {@inheritdoc}
     * Supports NormalUser objects.
     */
    public function supports($data, array $context = []): bool
    {
        return $data instanceof NormalUser;
    }

    /**
     * {@inheritdoc}
     */
    public function persist($data, array $context = [])
    {
        if (($data->getLongitude() || 0 == $data->getLongitude()) && ($data->getLatitude() || 0 == $data->getLatitude())) {
            $data->setLocation();
        }
        parent::persist($data, $context);
    }
}
