<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Chat;
use App\Entity\NormalUser;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;

/**
 * Data persister for Message objects.
 *
 * @author Louis Fahrenholz
 */
class ChatDataPersister implements ContextAwareDataPersisterInterface
{
    private $entityManager;
    private $security;

    public function __construct(EntityManagerInterface $entityManager, Security $security)
    {
        $this->entityManager = $entityManager;
        $this->security = $security;
    }

    /**
     * {@inheritdoc}
     */
    public function supports($data, array $context = []): bool
    {
        return $data instanceof Chat;
    }

    /**
     * {@inheritdoc}
     */
    public function persist($data, array $context = [])
    {
        $user = $this->security->getUser();
        if (!$user instanceof NormalUser) {
            throw new AuthenticationException();
        }
        if (!\in_array($user, $data->getUsers(), true)
            && isset($context['collection_operation_name'])
            && 'post' == $context['collection_operation_name']) {
            $data->addUser($user);
        }
        if (isset($context['collection_operation_name']) && 'post' == $context['collection_operation_name']) {
            $data->setLastModified(new DateTimeImmutable());
        }
        $data->setUsers(\array_unique($data->getUsers()));
        $this->entityManager->persist($data);
        $this->entityManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function remove($data, array $context = [])
    {
        $this->entityManager->remove($data);
        $this->entityManager->flush();
    }
}
