<?php

namespace App\DataPersister;

use App\Entity\AdminUser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * AdminUser DataPersister.
 *
 * @author Louis Fahrenholz
 */
class AdminUserDataPersister extends AbstractUserDataPersister
{
    public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $userPasswordEncoder)
    {
        parent::__construct($entityManager, $userPasswordEncoder);
    }

    /**
     * {@inheritdoc}
     * Supports AdminUser objects.
     */
    public function supports($data, array $context = []): bool
    {
        return $data instanceof AdminUser;
    }
}
