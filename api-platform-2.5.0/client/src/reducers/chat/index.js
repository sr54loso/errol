/**
 * @author Denis Paluca <dp46nivu@studserv.uni-leipzig.de>
 */

import { combineReducers } from 'redux';

function messages(state = [], action) {
    switch (action.type) {
        case 'CHAT_MESSAGES':
            return action.state;
        case 'CHAT_MESSAGES_DELETE':
            return state.filter(
                item => item['@id'] !== action.state['@id']
            );
        case 'CHAT_MESSAGES_NEW':
            let isNew = true;
            const newState = state.map((message, i) => {
                if (message['@id'] === action.state['@id']) {
                    isNew = false;
                    return action.state;
                }
                return message;
            });

            if (isNew) {
                newState.push(action.state);
            }
            return newState;
        default:
            return state;
    }
}

function channels(state = [], action) {
    switch (action.type) {
        case 'CHAT_CHANNELS':
            return action.state;
        case 'CHAT_CHANNELS_DELETE':
            return state.filter(
                item => item['@id'] !== action.state['@id']
            );
        case 'CHAT_CHANNELS_NEW':
            let isNew = true;
            const newState = state.map((channel, i) => {
                if (channel['@id'] === action.state['@id']) {
                    isNew = false;
                    return action.state;
                }
                return channel;
            });

            if (isNew) {
                newState.unshift(action.state);
            }

            return newState;
        default:
            return state;
    }
}

function selectedChannel(state = null, action) {
    switch (action.type) {
        case 'CHAT_CHANNELS_SELECT':
            return action.state;
        default:
            return state;
    }
}

export default combineReducers({ messages, channels, selectedChannel });