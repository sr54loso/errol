/**
 * @author Denis Paluca <dp46nivu@studserv.uni-leipzig.de>
 */

import { combineReducers } from 'redux';

export function open(state = false, action) {
    switch (action.type) {
        case 'UI_SNACKBAR_OPEN':
            return action.state;
        default:
            return state;
    }
}
export function message(state = '', action) {
    switch (action.type) {
        case 'UI_SNACKBAR_MESSAGE':
            return action.state;
        default:
            return state;
    }
}
export function variant(state = 'info', action) {
    switch (action.type) {
        case 'UI_SNACKBAR_VARIANT':
            return action.state;
        default:
            return state;
    }
}

export default combineReducers({ open, message, variant });