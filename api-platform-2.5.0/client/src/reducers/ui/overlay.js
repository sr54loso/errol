/**
 * @author Denis Paluca <dp46nivu@studserv.uni-leipzig.de>
 */

import { combineReducers } from 'redux';

export function loading(state = false, action) {
    switch (action.type) {
        case 'UI_OVERLAY_LOADING':
            return action.state;
        default:
            return state;
    }
}

export default combineReducers({ loading });