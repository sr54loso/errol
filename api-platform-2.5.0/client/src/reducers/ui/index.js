import { combineReducers } from 'redux';
import overlay from './overlay';
import snackbar from './snackbar';

export default combineReducers({ overlay, snackbar });
