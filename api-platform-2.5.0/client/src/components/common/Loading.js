/**
 * @author Denis Paluca <dp46nivu@studserv.uni-leipzig.de>
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Backdrop, CircularProgress } from '@material-ui/core';

class Loading extends Component {
    static propTypes = {
        loading: PropTypes.bool.isRequired,
    };

    render() {

        return (
            <Backdrop
                style={{
                    zIndex: 10000,
                    color: '#fff',
                }}
                open={this.props.loading}
            >
                <CircularProgress />
            </Backdrop>
        );
    }
}

const mapStateToProps = state => {
    const { loading } = state.ui.overlay;
    return { loading };
};

export default connect(
    mapStateToProps,
    null
)(Loading);
