/**
 * @author Denis Paluca <dp46nivu@studserv.uni-leipzig.de>
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Snackbar, SnackbarContent, IconButton } from '@material-ui/core';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import CloseIcon from '@material-ui/icons/Close';
import WarningIcon from '@material-ui/icons/Warning';
import { close } from '../../actions/ui/snackbar';
import { amber, green, red, blue } from '@material-ui/core/colors';

const variantIcon = {
    success: CheckCircleIcon,
    warning: WarningIcon,
    error: ErrorIcon,
    info: InfoIcon,
};

const style = {
    success: {
        backgroundColor: green[600],
    },
    error: {
        backgroundColor: red[600],
    },
    info: {
        backgroundColor: blue[600],
    },
    warning: {
        backgroundColor: amber[700],
    },
}

class BaseSnackbar extends Component {
    static propTypes = {
        open: PropTypes.bool.isRequired,
        message: PropTypes.string,
        variant: PropTypes.oneOf(['error', 'info', 'success', 'warning']).isRequired,
    };

    render() {
        const { open, variant, message, onClose } = this.props;
        const Icon = variantIcon[variant];
        return (
            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                open={open}
                autoHideDuration={3000}
                onClose={onClose}
            >
                <SnackbarContent
                    style={style[variant]}
                    aria-describedby="client-snackbar"
                    message={
                        <span id="client-snackbar" style={{
                            display: 'flex',
                            alignItems: 'center',
                        }}>
                            <Icon style={{
                                fontSize: 20,
                                opacity: 0.9
                            }} />
                            {'  ' + message}
                        </span>
                    }
                    action={[
                        <IconButton key="close" aria-label="close" color="inherit" onClick={onClose}>
                            <CloseIcon />
                        </IconButton>,
                    ]}
                />
            </Snackbar>
        );
    }
}

const mapStateToProps = state => {
    const { open, message, variant } = state.ui.snackbar;
    return { open, message, variant };
};

const mapDispatchToProps = dispatch => ({
    onClose: () => dispatch(close()),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(BaseSnackbar);
