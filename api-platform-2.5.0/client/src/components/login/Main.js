/**
 * @author Denis Paluca <dp46nivu@studserv.uni-leipzig.de>
 */

import React from 'react';
import { Box, TextField, Button } from '@material-ui/core';
import Agent from '../../modules/authentication/Agent';

/**
 * Login component
 *
 * @class Main
 * @extends {Component}
 */
export class Main extends React.Component {
    /**
     * State of main
     * @property {string} email 
     * @property {string} passwort
     * @type {State}
     * @memberof Main
     */
    state = {
        email: '',
        password: ''
    }

    /**
     * Handles the onEnter event
     *  
     * @memberof Main
     */
    _onEnter = (e) => {
        if (e.key === 'Enter') {
            this._onLogin();
        }
    }

    /**
     * Handles the onLogin event
     * if the login is successful, pushes props to chat component
     * @constant email
     * @constant password
     * @constant loggedIn
     * @memberof Main
     */
    _onLogin = async () => {
        const { email, password } = this.state;
        const loggedIn = await Agent.login(email, password);
        if (loggedIn) {
            this.props.history.push('/')
        }
    }

    /**
     * Render function for the Login window
     *
     * @returns Rendered Login window
     * @memberof Main
     */
    render() {
        const { email, password } = this.state;
        return (
            <div className='container' style={{
                height: "100vh",
                display: "flex",
                justifyContent: "center",
                alignItems: "center"
            }}>
                <div className='container-wrapper' style={{
                    maxWidth: "325px",
                    width: "100%",
                    border: "1px solid #ededed",
                    borderRadius: "3px",
                    boxShadow: "0 1px 3px rgba(0,0,0,.25)",
                    backgroundColor: "#ffffff",
                }}>
                    <div className='container-header' style={{
                        padding: "1em",
                    }} />
                    <Box>
                        <div className='field-wrapper' style={{
                            width: "100%",
                            position: "relative",
                            paddingTop: "1.75em",
                            backgroundColor: "#ffffff",
                        }}>
                            <TextField fullWidth
                                label={'Email'}
                                value={email}
                                type={'email'}
                                onKeyPress={this._onEnter}
                                onChange={e => this.setState({ email: e.target.value })}
                            />
                        </div>
                        <br />
                        <div className='field-wrapper' style={{
                            width: "100%",
                            position: "relative",
                            paddingTop: "1.75em",
                            backgroundColor: "#ffffff",
                        }}>
                            <TextField fullWidth
                                label={'Password'}
                                value={password}
                                type={'password'}
                                onKeyPress={this._onEnter}
                                onChange={e => this.setState({ password: e.target.value })}
                            />
                        </div>
                        <br />
                        <div className='button-wrapper' style={{
                            padding: "1em 1em",
                            width: "100%",
                            backgroundColor: "#fff",
                            display: "flex",
                            justifyContent: "center",
                            border: "#fff",
                        }}>
                            <Button
                                onClick={this._onLogin}
                            > Login </Button>
                        </div>
                    </Box>
                </div>
                <div className='background-div' style={{
                    backgroundImage: "url(https://web.whatsapp.com/img/bg-chat-tile_9e8a2898faedb7db9bf5638405cf81ae.png)",
                    backgroundRepeat: "repeat",
                    position: "absolute",
                    top: "0",
                    height: "100%",
                    width: "100%",
                    opacity: ".06",
                    zIndex: "-1",
                }} />
            </div>
        );
    }
}
export default Main;
