import React from 'react';
import { connect } from 'react-redux';
import { list, selectChannel } from '../../actions/chat/list';
import Agent from '../../modules/authentication/Agent';
import { MessageList, ChatList, Input } from 'react-chat-elements';
import { Button, TextField } from '@material-ui/core';
import 'react-chat-elements/dist/main.css';
import './Main.css';
import UserBar from './UserBar';
import { MessagesProvider } from '../../modules/providers';
import ChatHeader from './ChatHeader';

/**
 * Chat component
 *
 * @class Main
 * @extends {React.Component}
 */
class Main extends React.Component {
    /**
     * Default state
     * @type {State}
     * @memberof Main
     */
    state = {
        filter: ''
    }
    /**
     *
     *
     * @memberof Main
     */
    componentDidMount() {
        if (!Agent.isLoggedIn) {
            this.props.history.push('/login');
            return;
        }
        this.props.list();
    }

    /**
     * Handles the sendMessage event
     *
     * @memberof Main
     */
    _sendMessage = () => {
        const { selectedChannel } = this.props;
        const message = this.refs.input.input.value;
        MessagesProvider.sendText(message, selectedChannel);
        this.refs.input.clear();
    }

    _getRandomColor = () => {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    _onSelectChannel = ({ data }) => {
        for (const user of data.users) {
            this._userColorMap[user['@id']] = this._getRandomColor();
        }
        this.props.selectChannel(data);
    }

    _userColorMap = {};

    /**
     * Render function for displaying the chat window
     *
     * @returns Rendered chat window
     * @memberof Main
     */
    render() {
        const { channels, messages, selectedChannel } = this.props;
        const { filter } = this.state;
        return (
            <div className='container'>
                <div className="sidebar-elements">
                    <UserBar />
                    <TextField
                        id="filled-search"
                        type="search"
                        variant="filled"
                        label="Search chats"
                        value={filter}
                        style={{
                            background: 'white',
                            height: '56px'
                        }}
                        size="small"
                        onChange={e => this.setState({ filter: e.target.value })}
                    />
                    <div style={{
                        overflow: 'auto',
                        height: 'calc(100% - 120px)'
                    }}>
                        <ChatList
                            onClick={this._onSelectChannel}
                            dataSource={
                                channels
                                    .filter(c => !filter || c.name.toLowerCase().indexOf(filter.toLowerCase()) > -1)
                                    .map(c => ({
                                        title: c.name,
                                        data: c,
                                        date: null,
                                        alt: c.name.substring(0, 1).toUpperCase()
                                    }))
                            } />
                    </div>
                </div>

                <div className="background-div" />

                <div className='right-panel'>
                    <ChatHeader />
                    <MessageList
                        className='message-list'
                        lockable={true}
                        dataSource={
                            messages
                                .filter(m => selectedChannel && (m.chat === selectedChannel['@id']))
                                .map(m => {
                                    const user = selectedChannel.users.filter(u => u['@id'] === m.sender).pop();
                                    let username = m.sender;
                                    if (user && user.username) {
                                        username = user.username
                                    }

                                    const message = {
                                        position: m.sender === Agent.user.iri ? 'right' : 'left',
                                        type: 'text',
                                        text: m.content,
                                        date: new Date(m.timestamp),
                                        view: 'list',
                                        titleColor: this._userColorMap[m.sender]
                                    }

                                    if (!selectedChannel.isNull) {
                                        message.title = username;
                                    }

                                    return message;
                                })
                        } />

                    {selectedChannel && <Input
                        placeholder="Type a message"
                        defaultValue=""
                        ref='input'
                        maxlength={140}
                        multiline={true}
                        // buttonsFloat='left'
                        onKeyPress={(e) => {
                            if (e.shiftKey && e.charCode === 13) {
                                return true;
                            }
                            if (e.charCode === 13) {
                                this._sendMessage();
                                e.preventDefault();
                                return false;
                            }
                        }}
                        rightButtons={
                            <Button
                                variant="contained"
                                color="primary"
                                style={{ background: "#4E6775" }}
                                onClick={this._sendMessage}
                            >
                                Senden
                            </Button>
                        }
                    />}
                </div>
            </div>
        )
    }
}

/**
 * function mapping state to props
 * @constant
 * @param {*} state
 * @returns channels, messages, selectedChannel
 */
const mapStateToProps = (state, props) => {
    const {
        channels,
        messages,
        selectedChannel
    } = state.chat;

    return { channels, messages, selectedChannel, history: props.history };
};

/**
 * function mapping dispatch to props
 *
 * @param {*} dispatch
 */
const mapDispatchToProps = dispatch => ({
    list: () => dispatch(list()),
    selectChannel: channel => dispatch(selectChannel(channel))
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);