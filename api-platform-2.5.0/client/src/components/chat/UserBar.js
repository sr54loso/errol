import React from 'react';
import {
    Menu, MenuItem, AppBar, Toolbar, Avatar, Typography, IconButton, List, Button, TextField,
    ListItem, ListItemAvatar, DialogActions, ListItemText, Dialog, DialogTitle, Checkbox, ListItemIcon, DialogContent,
    InputBase, Divider, Paper, Chip
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import ChatIcon from '@material-ui/icons/Chat';
import SearchIcon from '@material-ui/icons/Search';
import Agent from '../../modules/authentication/Agent';
import { withRouter } from 'react-router-dom';
import { ChatsProvider, UsersProvider } from '../../modules/providers';
import { simplifyDistance } from '../../utils/conversionUtils';
import { selectChannel } from '../../actions/chat/list';
import { create } from '../../actions/ui/snackbar';
import { connect } from 'react-redux';
import ProfileDialog from './ProfileDialog';
import { uniqBy } from 'lodash';

/**
 *  UserBar component
 *
 * @class UserBar
 * @extends {React.Component}
 */
class UserBar extends React.Component {
    /**
     * Default State of Userbar
     * @type {State}
     * @memberof UserBar
     */
    state = {
        anchorEl: null,
        dialogOpen: false,
        dialogType: 'chat', //values 'chat' | 'group'
        users: [],
        selectedUsers: [],
        groupName: '',
        searchText: '',
        userDialog: false
    }
    _allUsers;

    /**
     * Handles the onClick event
     *
     * @memberof UserBar
     */
    _onClick = event => {
        this.setState({ anchorEl: event.currentTarget });
    };

    /**
     * Handles the Menu onClose event
     *
     * @memberof UserBar
     */
    _onCloseMenu = () => {
        this.setState({ anchorEl: null });
    };

    /**
     * Handles the onLogout event
     *
     * @memberof UserBar
     */
    _onLogut = () => {
        localStorage.clear();
        this.props.history.push('/login');
        this._onCloseMenu();
    }


    /**
     * Handles the showDialog event
     *
     * @memberof UserBar
     */
    _showDialog = async (type) => {
        await UsersProvider.updateLocation();
        const users = (await UsersProvider.get())
            .filter(u => u.email !== Agent.user.email)
            .sort((a, b) => {
                if (a.distance === b.distance) {
                    return 0
                } else if (!a.distance && a.distance !== 0) {
                    return 1;
                } else if (!b.distance && b.distance !== 0) {
                    return -1;
                }

                return a.distance - b.distance;
            });

        this._allUsers = users;
        this.setState({
            users,
            dialogOpen: true,
            dialogType: type,
            anchorEl: null
        });
    }

    /**
     * Handles the startChat event
     *
     * @param {*} user
     * @memberof UserBar
     */
    _startChat(users, name = null) {
        const { channels, selectChannel } = this.props;
        if (name === null && users.length === 1) {
            const channel = channels.find(c => {
                if (!c.isNull) {
                    return false;
                }
                const filteredUsers = c.users.filter(u => u['@id'] !== Agent.user.iri);
                return filteredUsers.length === 1 && users[0]['@id'] === filteredUsers[0]['@id'];
            });

            if (channel) {
                selectChannel(channel);
            } else {
                ChatsProvider.create(name, users);
            }
        } else {
            ChatsProvider.create(name, users);
        }

        this._onCloseDialog();
    }

    _onSelectUser = user => {
        const { selectedUsers } = this.state;
        const inList = selectedUsers.find(u => u.id === user.id);
        let newUsers;
        if (inList) {
            newUsers = selectedUsers.filter(u => u.id !== user.id);
        } else {
            newUsers = selectedUsers.concat([user]);
        }

        this.setState({ selectedUsers: newUsers });
    }

    _onCloseDialog = () => {
        this._allUsers = undefined;
        this.setState({ dialogOpen: false, selectedUsers: [], groupName: '', searchText: '' });
    }

    _onNewGroup = () => {
        const { groupName, selectedUsers } = this.state;
        const { error } = this.props;

        if (!groupName) {
            error('A group must have a name');
            return;
        }

        this._startChat(selectedUsers, groupName);
    }

    _onSearch = async () => {
        const { searchText } = this.state;
        if (searchText) {
            const users = await UsersProvider.get(searchText);
            if (users) {
                this._allUsers = uniqBy(this._allUsers.concat(users), u => u.id);
                this.setState({ users });
            }
        }
    }

    _searchBar = () => (
        <Paper component="form" style={{
            padding: '2px 4px',
            display: 'flex',
            alignItems: 'center',
            width: 400,
        }}>
            <InputBase
                style={{
                    flex: 1,
                }}
                placeholder="Search Users"
                inputProps={{ 'aria-label': 'search users' }}
                value={this.state.searchText}
                onChange={(e) => {
                    const { value } = e.target;
                    this.setState({ searchText: value });
                    if (!value && this._allUsers) {
                        this.setState({ users: this._allUsers });
                    }
                }}
                onKeyPress={(e) => {
                    if (e.key === 'Enter') {
                        e.preventDefault();
                        this._onSearch();
                    }
                }}
            />
            <Divider style={{
                height: 28,
                margin: 4,
            }} orientation="vertical" />
            <IconButton
                type="submit"
                style={{
                    padding: 10,
                }}
                aria-label="search"
                onClick={(e) => {
                    if (e) {
                        e.preventDefault();
                    }
                    this._onSearch();
                }}
            >
                <SearchIcon />
            </IconButton>
        </Paper>
    );

    /**
     * Render function for the Userbar
     *
     * @returns Rendered Userbar
     * @memberof UserBar
     */
    render() {
        const { anchorEl, dialogOpen, users, dialogType, selectedUsers, groupName, userDialogOpen } = this.state;
        const isChat = dialogType === 'chat';
        return (
            <>
                <AppBar
                    className="sidebar-elements"
                    position="static"
                    color="default"
                    style={{ height: '64px' }}>
                    <Toolbar>
                        {Agent.user && (
                            <>
                                <Avatar
                                    aria-label="Eingeloggter Benutzer"
                                    color="red"
                                >
                                    {(Agent.user.username).substring(0, 1).toUpperCase()}
                                </Avatar>

                                <Typography variant="h6" color="inherit">
                                    {Agent.user.username}
                                </Typography>
                            </>
                        )}
                        <section>
                            <IconButton
                                aria-label="New Chat"
                                color="inherit"
                                onClick={() => this._showDialog('chat')}
                            >
                                <ChatIcon />
                            </IconButton>

                            <IconButton
                                aria-label="Menü"
                                color="inherit"
                                onClick={this._onClick}
                            >
                                <MenuIcon />
                            </IconButton>
                        </section>
                    </Toolbar>
                </AppBar>

                <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={this._onCloseMenu}
                >
                    <MenuItem onClick={() => this._showDialog('group')}>New group</MenuItem>
                    <MenuItem onClick={() => this.setState({ userDialogOpen: true })}> Profile </MenuItem>
                    <MenuItem onClick={this._onLogut}>Logout</MenuItem>
                </Menu>

                <Dialog
                    onClose={this._onCloseDialog}
                    aria-labelledby="simple-dialog-title"
                    open={dialogOpen}
                    maxWidth="sm"
                    fullWidth
                >
                    <DialogTitle id="simple-dialog-title">
                        {isChat ? 'New Chat' : 'Add group participant'}
                        {this._searchBar()}

                        <div className={{
                            display: 'flex',
                            flexWrap: 'wrap',
                        }}>
                            {selectedUsers.map(user => (
                                user ?
                                    <Chip key={user.id} label={user.username} style={{ margin: 2 }} />
                                    : <></>
                            ))}
                        </div>
                    </DialogTitle>
                    <DialogContent dividers>
                        <List>
                            {users && users.map(user => (
                                <ListItem
                                    button
                                    onClick={() => isChat ? this._startChat([user]) : this._onSelectUser(user)}
                                    key={user.email}
                                >
                                    {!isChat && (
                                        <ListItemIcon>
                                            <Checkbox
                                                edge="start"
                                                checked={selectedUsers.map(u => u.id).indexOf(user.id) !== -1}
                                            />
                                        </ListItemIcon>
                                    )}
                                    <ListItemAvatar>
                                        <Avatar>
                                            {JSON.stringify(user.username).substring(1, 2).toUpperCase()}
                                        </Avatar>
                                    </ListItemAvatar>
                                    <ListItemText
                                        primary={user.username}
                                        secondary={
                                            (user.distance || user.distance === 0) ?
                                                `distance: ${simplifyDistance(user.distance)}` :
                                                user.email
                                        }
                                    />
                                </ListItem>
                            ))}
                        </List>
                    </DialogContent>
                    {!isChat && (
                        <DialogActions>
                            <TextField
                                autoFocus
                                margin="dense"
                                label="Group Name"
                                fullWidth
                                value={groupName}
                                onChange={(e) => this.setState({ groupName: e.target.value })}
                                inputProps={{ maxLength: 64 }}
                                onKeyPress={(e) => {
                                    if (e.charCode === 13) {
                                        this._onNewGroup();
                                        e.preventDefault();
                                    }
                                }}
                            />
                            <Button
                                onClick={this._onNewGroup}
                                color="primary"
                                variant="contained"
                            >
                                New group
                            </Button>
                        </DialogActions>
                    )}
                </Dialog>

                <ProfileDialog open={userDialogOpen} closeDialog={() => this.setState({ userDialogOpen: false })} />
            </>
        );
    }
}

/**
 * function mapping state to props
 * @constant
 * @param {*} state
 * @returns channels, messages, selectedChannel
 */
const mapStateToProps = (state, ownProps) => {
    const { channels } = state.chat;
    const { history } = ownProps;
    return { history, channels };
};

/**
 * function mapping dispatch to props
 *
 * @param {*} dispatch
 */
const mapDispatchToProps = dispatch => ({
    selectChannel: channel => dispatch(selectChannel(channel)),
    error: message => dispatch(create({ variant: 'error', message }))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(UserBar));