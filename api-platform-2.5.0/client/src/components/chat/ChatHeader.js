import React from 'react';
import { connect } from 'react-redux';
import {
    ListItem, ListItemAvatar, ListItemText, Avatar, TextField, List,
    Dialog, DialogActions, DialogContent, DialogTitle, Chip, Button,
    ListItemIcon, Checkbox, Divider, InputBase, Paper, IconButton,
    ListItemSecondaryAction
} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import { cloneDeep, uniqBy, isEqual } from 'lodash';
import { UsersProvider, ChatsProvider } from '../../modules/providers';
import { Agent } from '../../modules/authentication';
import { simplifyDistance } from '../../utils/conversionUtils';
import { selectChannel } from '../../actions/chat/list';
import { create } from '../../actions/ui/snackbar'
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

class ChatHeader extends React.Component {
    state = {
        open: false,
        searchText: ''
    }
    _allUsers = [];
    _fetchUsers = async () => {
        await UsersProvider.updateLocation();
        const users = (await UsersProvider.get())
            .filter(u => u.email !== Agent.user.email)
            .sort((a, b) => {
                if (a.distance === b.distance) {
                    return 0
                } else if (!a.distance && a.distance !== 0) {
                    return 1;
                } else if (!b.distance && b.distance !== 0) {
                    return -1;
                }

                return a.distance - b.distance;
            });
        this._allUsers = users;
        this.setState({ users });
    }
    _onOpen = () => {
        const { selectedChannel } = this.props;
        if (!selectedChannel.isNull) {
            this._fetchUsers();
            this.setState({
                open: true,
                group: cloneDeep(selectedChannel)
            });
        }
    }

    _onClose = () => {
        this._allUsers = [];
        this.setState({ open: false, searchText: '' });
    }

    _onDelete = async () => {
        await ChatsProvider.delete(this.props.selectedChannel);
        this._onClose();
    }

    _onLeave = async () => {
        await ChatsProvider.leave(this.props.selectedChannel);
        this.props.selectChannel(null);
        this._onClose();
    }

    _onSave = async () => {
        const { selectedChannel, error } = this.props;
        const { group } = this.state;
        if (!isEqual(group, selectedChannel)) {
            if (!group.name) {
                error('A group must have a name');
                return;
            }
            await ChatsProvider.update(this.state.group)
        }
        this._onClose();
    }


    _onSearch = async () => {
        const { searchText } = this.state;
        if (searchText) {
            const users = await UsersProvider.get(searchText);
            if (users) {
                this._allUsers = uniqBy(this._allUsers.concat(users), u => u.id);
                this.setState({ users });
            }
        }
    }

    _searchBar = () => (
        <Paper component="form" style={{
            padding: '2px 4px',
            display: 'flex',
            alignItems: 'center',
            width: 400,
        }}>
            <InputBase
                style={{
                    flex: 1,
                }}
                placeholder="Search Users"
                inputProps={{ 'aria-label': 'search users' }}
                value={this.state.searchText}
                onChange={(e) => {
                    const { value } = e.target;
                    this.setState({ searchText: value });
                    if (!value && this._allUsers) {
                        this.setState({ users: this._allUsers });
                    }
                }}
                onKeyPress={(e) => {
                    if (e.key === 'Enter') {
                        e.preventDefault();
                        this._onSearch();
                    }
                }}
            />
            <Divider style={{
                height: 28,
                margin: 4,
            }} orientation="vertical" />
            <IconButton
                type="submit"
                style={{
                    padding: 10,
                }}
                aria-label="search"
                onClick={(e) => {
                    if (e) {
                        e.preventDefault();
                    }
                    this._onSearch();
                }}
            >
                <SearchIcon />
            </IconButton>
        </Paper>
    );

    render() {
        const { selectedChannel } = this.props;
        const { open, group, users } = this.state;
        return selectedChannel && (
            <>
                <ListItem
                    style={{
                        backgroundColor: 'white',
                        padding: 0,
                        height: '64px'
                    }}
                >
                    <ListItemAvatar>
                        <Avatar>
                            {JSON.stringify(selectedChannel.name).substring(1, 2).toUpperCase()}
                        </Avatar>
                    </ListItemAvatar>
                    <ListItemText
                        primary={selectedChannel.name}
                        secondary={selectedChannel.isNull ? '' : selectedChannel.users.map(u => u.username).join(", ")}
                    />
                    <ListItemIcon>
                        <IconButton onClick={this._onDelete}>
                            <DeleteIcon color="error" />
                        </IconButton>
                    </ListItemIcon>
                    {!selectedChannel.isNull && (<>
                        {selectedChannel.users.length !== 1 && (
                            <ListItemIcon>
                                <IconButton onClick={this._onLeave}>
                                    <ExitToAppIcon color="action" />
                                </IconButton>
                            </ListItemIcon>
                        )}

                        <ListItemIcon>
                            <IconButton onClick={this._onOpen}>
                                <EditIcon color="primary" />
                            </IconButton>
                        </ListItemIcon>
                    </>)}
                </ListItem>
                {group && (
                    <Dialog
                        onClose={this._onClose}
                        open={open}
                        maxWidth="sm"
                        fullWidth
                    >
                        <DialogTitle>
                            <TextField
                                label="Group Name"
                                value={group.name}
                                onChange={(e) => {
                                    group.name = e.target.value;
                                    this.setState({ group });
                                }}
                            />
                            <br />
                            <div className={{
                                display: 'flex',
                                flexWrap: 'wrap',
                            }}>
                                Members: {group.users.map(user => (
                                user['@id'] !== Agent.user.iri ?
                                    <Chip
                                        key={user.id}
                                        label={user.username}
                                        style={{ margin: 2 }}
                                        onDelete={() => {
                                            group.users = group.users.filter(u => u.id !== user.id);
                                            this.setState({ group });
                                        }}
                                    />
                                    : <></>
                            ))}
                            </div>
                            {this._searchBar()}
                            <Divider />
                        </DialogTitle>
                        <DialogContent>
                            <List>
                                {users && users.map(user => (
                                    <ListItem
                                        key={user.email}
                                        button
                                        onClick={() => {
                                            const found = group.users.find(u => u.id === user.id);
                                            if (found) {
                                                group.users = group.users.filter(u => u.id !== user.id);
                                            } else {
                                                group.users.push(user);
                                            }
                                            this.setState({ group });
                                        }}
                                    >
                                        <ListItemIcon>
                                            <Checkbox
                                                edge="start"
                                                checked={group.users.map(u => u.id).indexOf(user.id) !== -1}
                                            />
                                        </ListItemIcon>
                                        <ListItemAvatar>
                                            <Avatar>
                                                {JSON.stringify(user.username).substring(1, 2).toUpperCase()}
                                            </Avatar>
                                        </ListItemAvatar>
                                        <ListItemText
                                            primary={user.username}
                                            secondary={
                                                (user.distance || user.distance === 0) ?
                                                    `distance: ${simplifyDistance(user.distance)}` :
                                                    user.email
                                            }
                                        />
                                    </ListItem>
                                ))}
                            </List>
                        </DialogContent>

                        <DialogActions>
                            <DialogActions>
                                <Button onClick={this._onClose} color="primary">
                                    Cancel
                                </Button>
                                <Button onClick={this._onSave} color="primary">
                                    Save
                                </Button>
                            </DialogActions>
                        </DialogActions>
                    </Dialog>
                )}
            </>
        );
    }
}


/**
 * function mapping state to props
 * @constant
 * @param {*} state
 * @returns channels, messages, selectedChannel
 */
const mapStateToProps = state => {
    const {
        channels,
        selectedChannel
    } = state.chat;

    return { channels, selectedChannel };
};

/**
 * function mapping dispatch to props
 *
 * @param {*} dispatch
 */
const mapDispatchToProps = dispatch => ({
    selectChannel: channel => dispatch(selectChannel(channel)),
    error: message => dispatch(create({ variant: 'error', message }))
});

export default connect(mapStateToProps, mapDispatchToProps)(ChatHeader);