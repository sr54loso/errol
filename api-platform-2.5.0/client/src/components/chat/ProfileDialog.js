import React from 'react';
import { Agent } from '../../modules/authentication';
import { BaseProvider } from '../../modules/providers/BaseProvider';
import { AppBar, Button, Dialog, DialogContent, IconButton, Input, InputAdornment, InputLabel, Slide, TextField, Toolbar, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { Visibility, VisibilityOff } from '@material-ui/icons';


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

class ProfileDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = { email: Agent.user.email, username: Agent.user.username, showPassword: false, password: '' };
  }

  resetState = () => {
    this.setState({ email: Agent.user.email, username: Agent.user.username, showPassword: false, password: '' })
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleClickShowPassword = () => {
    this.setState({ showPassword: !this.state.showPassword });
  }

  handleMouseDownPassword = event => {
    event.preventDefault();
  };

  render() {
    const { open, closeDialog } = this.props;

    return (
      <Dialog fullScreen open={open} onClose={closeDialog} TransitionComponent={Transition} style={{ paddingTop: '300' }}>
        <AppBar style={{
          position: 'relative',
          backgroundColor: '#DDDDDD',
          color: 'black'
        }}>
          <Toolbar>
            <IconButton edge="start" color="inherit" onClick={() => { this.resetState(); closeDialog() }} aria-label="close">
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" style={{
              flex: 1,
            }}>
              Profile
            </Typography>
            <Button autoFocus color="inherit" onClick={() => {
              BaseProvider._put(Agent.user.iri,
                {
                  "username": this.state.username,
                  "email": this.state.email,
                  "password": this.state.password
                },
                false,
                {
                  error: "Could not update profile."
                });
              this.resetState();
              closeDialog()
            }}
            >
              Save
            </Button>
          </Toolbar>
        </AppBar>
        <div className="MuiDialogContent-root" style={{
          paddingTop: '27',
          paddingRight: '63'
        }}>
          <DialogContent>
            <TextField value={this.state.email} name="email" label="Email" onChange={this.handleChange} fullWidth />
          </DialogContent>
          <DialogContent>
            <TextField value={this.state.username} name="username" label="Username" onChange={this.handleChange} fullWidth />
          </DialogContent>
          <DialogContent>
            <InputLabel htmlFor="standard-adornment-password">Password</InputLabel>
            <Input
              id="standard-adornment-password"
              name="password"
              type={this.state.showPassword ? "text" : "password"}
              value={this.state.password}
              onChange={this.handleChange}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    name="showPassword"
                    aria-label="toggle password visibility"
                    onClick={this.handleClickShowPassword}
                    onMouseDown={this.handleMouseDownPassword}
                  >
                    {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              }
            />
          </DialogContent>
        </div>
      </Dialog >
    );
  };
};
export default ProfileDialog;

