import { Agent } from '../modules/authentication'

/**
 * @author Denis Paluca <dp46nivu@studserv.uni-leipzig.de>
 */
export function fixChatName(chat) {
    if (chat.name) {
        return chat;
    }

    return {
        ...chat,
        name: chat.users
            .filter(u => u['@id'] !== Agent.user.iri)
            .map(u => u.username)
            .join(', '),
        isNull: chat.users.length === 2
    }
}