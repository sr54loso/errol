/**
 * @author Denis Paluca <dp46nivu@studserv.uni-leipzig.de>
 */
export function simplifyDistance(distance) {
    const inKm = distance / 1000;
    if (inKm >= 1) {
        return `${inKm.toFixed(2)} km`;
    }

    const inM = distance / 1;
    return `${inM.toFixed(2)} m`
}