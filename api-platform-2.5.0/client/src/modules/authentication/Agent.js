/**
 * @author Denis Paluca <dp46nivu@studserv.uni-leipzig.de>
 */

import { AuthProvider } from '../providers';
import { TokenItem } from './TokenItem';

const TOKEN_STORAGE = 'token';
const USER_STORAGE = 'user';
const REFRESH_STORAGE = 'refresh';

/**
 * Agent component
 *
 * @export
 * @class Agent
 */
export class Agent {
    /**
     * sets the token from sessionStorage
     * @param {string} _token  
     * @protected
     * @static
     * @memberof Agent
     */
    static _tokenItem = new TokenItem(JSON.parse(localStorage.getItem(TOKEN_STORAGE)));

    /**
     * sets the user props from sessionStorage
     * @param {*} _user
     * @static
     * @memberof Agent
     */
    static _user = JSON.parse(localStorage.getItem(USER_STORAGE));

    static _refreshToken = localStorage.getItem(REFRESH_STORAGE);

    /**
     * Getter for the Token
     * @public
     * @readonly
     * @static
     * @memberof Agent
     */
    static async acquireToken() {
        if (!this._tokenItem.isExpired) {
            return this._tokenItem.token;
        }

        const authData = await AuthProvider.refresh(this._refreshToken);
        this._setTokens(authData)

        return this._tokenItem.token;
    }

    /**
     * Getter for the user
     *
     * @readonly
     * @static
     * @memberof Agent
     */
    static get user() {
        return this._user;
    }

    static get isLoggedIn() {
        return !!this._refreshToken;
    }

    static _setTokens(authData) {
        const { token, user, refresh_token, expiresOn } = authData;

        this._tokenItem = new TokenItem({ token, expiresOn });
        this._user = user;
        this._refreshToken = refresh_token;
        localStorage.setItem(TOKEN_STORAGE, this._tokenItem.stringify());
        localStorage.setItem(USER_STORAGE, JSON.stringify(this._user));
        localStorage.setItem(REFRESH_STORAGE, this._refreshToken);
    }

    /**
     * Logs in user and saves token to session storage. Returns true in successful, false if not
     * 
     * @public
     * @static
     * @param {*} email
     * @param {*} password
     * @returns {boolean} true in successful, false if not
     * @memberof Agent
     */
    static async login(email, password) {
        try {
            const authData = await AuthProvider.login(email, password);
            this._setTokens(authData);
            return true;
        } catch (e) {
            return false;
        }
    }
}

export default Agent;