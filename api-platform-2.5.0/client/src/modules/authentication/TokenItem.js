/**
 * @author Denis Paluca <dp46nivu@studserv.uni-leipzig.de>
 */
export class TokenItem {
    _token;
    _expiresOn;
    get isExpired() {
        if (Date.now() >= this._expiresOn * 1000) {
            return true;
        }

        return false;
    }

    stringify() {
        return JSON.stringify({
            token: this._token,
            expiresOn: this._expiresOn
        });
    }

    get token() {
        return this._token;
    }

    constructor(data) {
        if (data) {
            this._token = data.token;
            this._expiresOn = data.expiresOn;
        }
    }
}

export default TokenItem;