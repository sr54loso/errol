import { BaseProvider } from '.';
import { Agent } from '../authentication';
import { create } from '../../actions/ui/snackbar';
/**
 * @author Denis Paluca <dp46nivu@studserv.uni-leipzig.de>
 */
/**
 * User Provider
 *
 * @export
 * @class UsersProvider
 * @extends {BaseProvider}
 */
export class UsersProvider extends BaseProvider {
    /**
     * Getter for Users
     *
     * @static
     * @param {boolean} [suppressLoading=false]
     * @returns Users
     * @memberof UsersProvider
     */
    static async get(username = null, suppressLoading = false) {
        return this._get(
            !username ? 'users' : 'users?username=' + username,
            suppressLoading,
            {
                error: 'Could not get users'
            }
        );
    }
    
/**
 * function to get the currently logged in user
 *
 * @static
 * @param {boolean} [suppressLoading=false]
 * @returns current logged in user
 * @memberof UsersProvider
 */
static async getCurrentUser(suppressLoading=false){
        return this._get(
            Agent.user.iri,
            suppressLoading,
            {
                error: 'Could not get current user'
            }
        );
    }

    static async updateLocation(suppressLoading = false) {
        let location = {
            latitude: null,
            longitude: null
        }

        async function fetchLocation() {
            const response = await fetch('https://location.services.mozilla.com/v1/geolocate?key=test');
            const data = await response.json();
            const { lat, lng } = data.location;

            return {
                latitude: lat,
                longitude: lng
            }
        }

        const { dispatch } = this.store;
        if (navigator.geolocation) {
            const promise = new Promise((resolve, reject) => navigator.geolocation.getCurrentPosition(resolve, reject));
            try {
                const position = await promise;
                const { latitude, longitude } = position.coords;
                location = {
                    latitude,
                    longitude
                }
            } catch (e) {
                try {
                    location = await fetchLocation();
                } catch (error2) {
                    dispatch(create({
                        message: e.message,
                        variant: 'error'
                    }));
                }
            }
        } else {
            try {
                location = await fetchLocation();
            } catch {
                dispatch(create({
                    message: 'Browser doesn\'t support geo location',
                    variant: 'info'
                }))
            }
        }

        await this._put(
            Agent.user.iri,
            location,
            suppressLoading,
            {
                error: `Could not update position of user: ${Agent.user.email}`
            }
        );
    }
}