import { BaseProvider } from '.';
import jwtDecode from 'jwt-decode';
/**
 * @author Denis Paluca <dp46nivu@studserv.uni-leipzig.de>
 */
/**
 * Authentification Provider
 *
 * @export
 * @class AuthProvider
 * @extends {BaseProvider}
 */
export class AuthProvider extends BaseProvider {
    /**
     *
     *
     * @static
     * @param {*} email
     * @param {*} password
     * @param {boolean} [suppressLoader=false]
     * @returns jwt token, mercure token, user data
     * @memberof AuthProvider
     */
    static async login(email, password, suppressLoader = false) {
        const data = await this._execute(
            'authentication_token',
            {
                method: 'POST',
                credentials: 'include',
                body: JSON.stringify({
                    email,
                    password
                })
            },
            suppressLoader,
            {
                error: `Could not log in with email: ${email}`,
                success: `User ${email} logged in.`
            }
        );

        return this._parseAuthData(data);
    }

    static async refresh(refreshToken, suppressLoader = false) {
        const data = await this._execute(
            'refresh_token',
            {
                method: 'POST',
                credentials: 'include',
                body: JSON.stringify({
                    refresh_token: refreshToken
                })
            },
            suppressLoader,
            {
                error: `User credentials error!`,
            }
        );

        return this._parseAuthData(data);
    }

    static _parseAuthData(data) {
        const { token, mercure, user_iri, refresh_token, username } = data;
        const { roles, email, exp } = jwtDecode(token);

        return {
            token,
            mercure,
            refresh_token,
            expiresOn: exp,
            user: {
                email,
                role: roles && (roles[1] || roles[0]),
                iri: user_iri,
                username
            }
        }
    }
}
