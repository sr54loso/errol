import { BaseProvider } from '.';
/**
 * @author Denis Paluca <dp46nivu@studserv.uni-leipzig.de>
 */
/**
 * Message Provider
 *
 * @export
 * @class MessagesProvider
 * @extends {BaseProvider}
 */
export class MessagesProvider extends BaseProvider {
    /**
     * Getter for messages
     *
     * @static
     * @param {*} chats
     * @param {boolean} [suppressLoading=false]
     * @returns Messages
     * @memberof MessagesProvider
     */
    static async get(chats, suppressLoading = false) {
        const searchParams = new URLSearchParams();
        for (const chat of chats) {
            searchParams.append('chat[]', chat['@id']);
        }

        return this._get(
            'messages?' + searchParams.toString(),
            suppressLoading,
            {
                error: 'Could not get messages'
            }
        );
    }

    /**
     * Function for sending a text message
     *
     * @static
     * @param {*} message
     * @param {*} chat
     * @param {boolean} [suppressLoading=false]
     * @memberof MessagesProvider
     */
    static async sendText(message, chat, suppressLoading = false) {
        message = message.trim();
        if (message) {
            this._post(
                'messages',
                {
                    content: message,
                    chat: chat['@id'],
                    type: 'text'
                },
                suppressLoading,
                {
                    error: `Could not send message: ${message} to chat: ${chat.name}`
                }
            );
        }
    }
}