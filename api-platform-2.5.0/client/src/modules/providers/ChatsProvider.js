import { Agent } from '../authentication';
import { BaseProvider } from '.';
import { fixChatName } from '../../utils/chatUtils';
import { selectChannel } from '../../actions/chat/list';
/**
 * @author Denis Paluca <dp46nivu@studserv.uni-leipzig.de>
 */
/**
 * Provider for Chats component
 *
 * @export
 * @class ChatsProvider
 * @extends {BaseProvider}
 */
export class ChatsProvider extends BaseProvider {

    static async update(chat, suppressLoading = false) {
        const { name, users } = chat;
        const updatedChat = await this._put(
            chat['@id'],
            {
                name,
                users: users.map(u => u['@id'])
            },
            suppressLoading,
            {
                success: `Group ${name} updated`,
                error: `Failed to update group ${name}`
            }
        );
        if (updatedChat) {
            this.store.dispatch(selectChannel(updatedChat));
        }
    }

    static async leave(chat, suppressLoading = false) {
        const leftChannel = await this._put(
            chat['@id'],
            {
                users: chat.users
                    .filter(u => u['@id'] !== Agent.user.iri)
                    .map(u => u['@id'])
            },
            suppressLoading,
            {
                success: `Left group ${chat.name}`,
                error: `Could not leave group ${chat.name}`
            }
        );

        if (leftChannel) {
            this.store.dispatch(selectChannel(null));
            this.store.dispatch({ type: 'CHAT_CHANNELS_DELETE', state: leftChannel });
        }
    }

    static async delete(chat, suppressLoading = false) {
        this.store.dispatch(selectChannel(null));
        return this._execute(
            chat['@id'],
            {
                method: 'DELETE',
            },
            suppressLoading
        )
    }

    /**
     * Getter for chats
     *
     * @static
     * @param {boolean} [suppressLoading=false]
     * @returns Chats
     * @memberof ChatsProvider
     */
    static async get(suppressLoading = false) {
        const chats = await this._get(
            `chats?users=${Agent.user.iri}`,
            suppressLoading,
            {
                error: 'Could not get chats'
            }
        );

        if (chats) {
            return chats.map(fixChatName);
        }
    }

    /**
     * Function for creating a chat
     *
     * @static
     * @param {*} name
     * @param {*} users
     * @param {boolean} [suppressLoading=false]
     * @memberof ChatsProvider
     */
    static async create(name, users, suppressLoading = false) {
        const usernames = users.map(u => u.username).join(', ');
        const newChat = await this._post(
            'chats',
            {
                name,
                users: users.map(user => `/users/${user.id}`)
            },
            suppressLoading,
            {
                error: `Could not create chat: ${name} with users: ${usernames}`,
                success: `Chat ${name || ''} created with users: ${usernames}`
            }
        );
        if (newChat) {
            this.store.dispatch(selectChannel(fixChatName(newChat)));
        }
    }
}