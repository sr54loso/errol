import { fetch, normalize } from '../../utils/dataAccess';
import { loading } from '../../actions/ui/overlay';
import { create } from '../../actions/ui/snackbar';
import { Agent } from '../authentication'

/**
 * @author Denis Paluca <dp46nivu@studserv.uni-leipzig.de>
 */
/**
 * Base Provider, Super class providing basic functions for other Providers
 *
 * @export
 * @class BaseProvider
 */
export class BaseProvider {
    static store;

    /**
     *
     *
     * @static
     * @param {*} resource
     * @param {boolean} [suppressLoading=false]
     * @param {*} [messages={}]
     * @returns normalized data
     * @memberof BaseProvider
     */
    static async _get(resource, suppressLoading = false, messages = {}) {
        const data = await this._execute(resource, { method: 'GET' }, suppressLoading, messages);

        return normalize(data)['hydra:member'];
    }

    /**
     *
     *
     * @static
     * @param {*} resource
     * @param {*} body
     * @param {boolean} [suppressLoading=false]
     * @param {*} [messages={}]
     * @returns Post data
     * @memberof BaseProvider
     */
    static async _post(resource, body, suppressLoading = false, messages = {}) {
        return this._execute(resource, {
            method: 'POST',
            body: JSON.stringify(body)
        }, suppressLoading, messages);
    }

    static async _put(resource, body, suppressLoading = false, messages = {}) {
        return this._execute(resource, {
            method: 'PUT',
            body: JSON.stringify(body)
        }, suppressLoading, messages);
    }

    static async _execute(resource, options = {}, suppressLoading = false, messages = {}) {
        const { dispatch } = this.store;
        try {
            !suppressLoading && dispatch(loading(true));

            if (resource !== 'authentication_token' && resource !== 'refresh_token') {
                options.headers = new Headers();
                options.headers.set('Authorization', 'Bearer ' + await Agent.acquireToken());
            }

            const response = await fetch(resource, options);
            const responseBody = await response.json();

            messages.success && dispatch(create({
                variant: 'success',
                message: messages.success
            }));

            return responseBody;
        } catch (error) {
            messages.error && dispatch(create({
                variant: 'error',
                message: messages.error
            }));
        } finally {
            !suppressLoading && dispatch(loading(false));
        }
    }
}