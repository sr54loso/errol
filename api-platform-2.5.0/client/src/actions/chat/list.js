import {
  normalize,
  subscribeAll
} from '../../utils/dataAccess';
import { create } from '../ui/snackbar'
import { ChatsProvider, MessagesProvider } from '../../modules/providers';
import { fixChatName } from '../../utils/chatUtils';

function messages(messages) {
  return { type: 'CHAT_MESSAGES', state: messages };
}

function channels(channels) {
  return { type: 'CHAT_CHANNELS', state: channels };
}

function mercureMessage(message) {
  return dispatch => {
    if (1 === Object.keys(message).length) {
      dispatch({ type: 'CHAT_MESSAGES_DELETE', state: message });
      return;
    }

    dispatch({ type: 'CHAT_MESSAGES_NEW', state: message });
  };
}

function mercureChannel(channel) {
  return dispatch => {
    if (1 === Object.keys(channel).length) {
      dispatch({ type: 'CHAT_CHANNELS_DELETE', state: channel });
      return;
    }

    dispatch({ type: 'CHAT_CHANNELS_NEW', state: fixChatName(channel) });
  };
}

export function list() {
  return async dispatch => {
    try {
      const chats = await ChatsProvider.get();
      dispatch(channels(chats));

      if (chats && chats.length > 0) {
        const retrievedMessages = await MessagesProvider.get(chats);
        dispatch(messages(retrievedMessages.reverse()));
        dispatch(selectChannel(chats[0]))
      }

      const eventSource = subscribeAll();
      eventSource.addEventListener('message', event => {
        const data = normalize(JSON.parse(event.data));
        const type = data['@id']
          .split('/')
          .filter(Boolean)[0];

        if (type === 'messages') {
          dispatch(mercureMessage(data));
        } else {
          dispatch(mercureChannel(data));
        }
      });
    } catch (e) {
      dispatch(create({
        message: 'Error ' + e,
        variant: 'error'
      }));
    }
  }
}

export function selectChannel(channel) {
  return dispatch => dispatch({ type: 'CHAT_CHANNELS_SELECT', state: channel });
}
