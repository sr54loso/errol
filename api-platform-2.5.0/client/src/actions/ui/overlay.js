/**
 * @author Denis Paluca <dp46nivu@studserv.uni-leipzig.de>
 */

export function loading(loading) {
    return { type: 'UI_OVERLAY_LOADING', state: loading };
}