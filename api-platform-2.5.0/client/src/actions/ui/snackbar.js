/**
 * @author Denis Paluca <dp46nivu@studserv.uni-leipzig.de>
 */

function open(open) {
    return { type: 'UI_SNACKBAR_OPEN', state: open }
}

function message(message) {
    return { type: 'UI_SNACKBAR_MESSAGE', state: message }
}

function variant(variant) {
    return { type: 'UI_SNACKBAR_VARIANT', state: variant }
}

export function create(payload) {
    return dispatch => {
        dispatch(open(true));
        dispatch(message(payload.message));
        dispatch(variant(payload.variant));
    }
}


export function close() {
    return dispatch => {
        dispatch(open(false));
        dispatch(message(''));
        dispatch(variant('info'));
    }
}