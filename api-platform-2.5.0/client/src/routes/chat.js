/**
 * @author Denis Paluca <dp46nivu@studserv.uni-leipzig.de>
 */

import React from 'react';
import { Route } from 'react-router-dom';
import { Main } from '../components/chat';

export default [
    <Route path="/" component={Main} key="chat" exact strict />,
];
