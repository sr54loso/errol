/**
 * @author Denis Paluca <dp46nivu@studserv.uni-leipzig.de>
 */

import React from 'react';
import { Route } from 'react-router-dom';
import { Main } from '../components/login';

export default [
    <Route path="/login" component={Main} key="login" />,
];
